"""
Module that holds the classes for checking event messages sent by client or
server devices.

@license: Licensed under the MIT license
    http://opensource.org/licenses/mit-license.php
@author: Michael Weinrich <testsuite@michael-weinrich.de>
@copyright: Copyright 2007, Michael Weinrich
"""

# external includes
import louie
import urllib2
from lxml import etree
from StringIO import StringIO
from twisted.internet import reactor
from twisted.python.filepath import FilePath
# Coherence includes
from coherence import log
from coherence.upnp.core import event
from coherence.upnp.devices.control_point import ControlPoint

global control_point, event_server
control_point = None
"""Global variable for a single instance of a C{ControlPoint}."""
event_server = None
"""Global variable for a single instance of an C{EventServer}."""


class ServerEventMessageChecks(log.Loggable):
    """
    This class contains event message checks for server devices like
    MediaServer and MediaRenderer for instance. It runs as part of a virtual
    client device (like a control point) and checks notify messages for
    validity.

    The main task is to listen to the C{UPnT.event.server_message_received}
    signal and process the data given by this signal.
    """

    logCategory = 'UPnT_ServerEventMessageChecks'
    """Log category for the server event checker."""

    def __init__(self, service, config):
        """
        Initialise the server event messsage checker.

        @param service: The service where this checker will subscribe for event
        messages.
        @param config: A C{dict} containing the configuration data from the
        config file.
        """

        self.notifySeq = {}
        """Storage for sequence numbers to enable tracking of sequence numbers
        in event messages."""
        self._service = service
        """The service whose event messages will be checked."""

        self._xmlschema = None
        """An XML schema that is used to validate the content of an event
        message."""
        schema_file_dir = config.get('schema_file_dir', FilePath(__file__).parent().parent().child('schemas').path)

        if not schema_file_dir.endswith('/'):
            schema_file_dir += '/'

        if schema_file_dir is not None:
            xmlschema_doc = etree.parse(schema_file_dir + 'event_notify.xsd')
            self._xmlschema = etree.XMLSchema(xmlschema_doc)

        self._host = ''
        """The host where the observed event server is running."""
        self._renew_func = None
        """An object which provides C{IDelayedCall} for L{renewSubscription}
        which renews the subscription at the event server every 290 seconds."""
        self.isHostAvailable()

    def isHostAvailable(self):
        """
        Check if the parent device is already available. If not, try again in
        two seconds. If so, get the host IP, connect the signal to the
        processing function, get the control point and event server and
        subscribe for event messages.
        """
        if self._service.parentDevice is None:
            reactor.callLater(2, self.isHostAvailable)
        else:
            self._host = self._service.parentDevice.host.ip
            louie.connect(self.checkEventMessage, 'UPnP.Event.Server.message_received', louie.Any, weak=False)
            global control_point, event_server
            if not control_point:
                if not self._service.parentDevice.host._coherence.ctrl:
                    control_point = ControlPoint(self._service.parentDevice.host._coherence)
                else:
                    control_point = self._service.parentDevice.host._coherence.ctrl
                self.debug('ControlPoint created')
            if not event_server:
                event_server = event.EventServer(control_point)
                self.debug('EventServer created')
            self.renewSubscription()
            self.debug('ServerEventMessageChecks initialized for service %r' % str(self._service))

    def renewSubscription(self):
        """
        Renew the subscription for event messages and schedule the next renewal
        in 290 seconds.
        """
        event.subscribe(self._service)
        self._renew_func = reactor.callLater(290, self.renewSubscription)

    def cancelSubscription(self):
        """
        Cancel the subscription for event messages and also cancel the scheduled
        renewal.
        """
        if self._renew_func.active():
            self._renew_func.cancel()
        event.unsubscribe(self._service)

    def checkEventMessage(self, command, header, body):
        """
        Processing function that is called when a
        C{UPnT.event.server_message_received} signal is received.

        If the command in the event packet is known, the L{checkNotifyMessage}
        function is called. Otherwise the erroneous packet will be displayed.

        @param command: A C{dict} for the command line of the event packet with
        keys 'method', 'resource' and 'protocol'.
        @param header: A C{dict} containing all headers from the event packet
        except the command line.
        @param body: C{str} containing the body of the event packet.
        """
        #self.debug('checkEventMessage (%r)' % packet_data)
        self.debug('checkEventMessage')
        if command['method'].lower() == 'notify':
            self.checkNotifyMessage(command, header, body)
        else:
            self.debug(command)
            self.debug(header)
            self.debug(body)

    def calcNextEventSeq(self, service_ident, service_sid):
        """
        Check for the next sequence id. If none is stored, create a new entry
        and start counting at 0. If there is already one stored, increment it
        and wrap it to 1 if it exceeded the maximum value of 429496729.

        @param service_ident: A unique identification that consists of the host
        IP and the path of the event server.
        @param service_sid: The unique identifier of the subscription.

        @return: The new sequence value (C{int}).
        """
        self.debug('calcNextEventSeq')

        # check for dict index
        if not self.notifySeq.has_key(service_ident):
            self.notifySeq[service_ident] = {}
        if not self.notifySeq[service_ident].has_key(service_sid):
            self.notifySeq[service_ident][service_sid] = 0
        else:

            # calc new value, wrap to one if value too big (UDA S.67)
            self.notifySeq[service_ident][service_sid] += 1
            if self.notifySeq[service_ident][service_sid] > 429496729:
                self.notifySeq[service_ident][service_sid] = 1
        #self.debug('calcNextEventSeq (%r, %r, %r, %r)' % (service_host, service_url, service_sid, self.notifySeq[service_ident][service_sid]))

    def removeEventSeq(self):
        """
        Remove the sequence number from the dictionary so that it starts again
        at 0 when we subscribe to event messages of a device the next time.
        """

        self.debug('removeEventSeq (%r)' % self.notifySeq)
        del self.notify_seq[self._host + self._service.event]

    def checkNotifyMessage(self, command, header, body):
        """
        Check the headers of the event message. The body is checked in the
        L{validateMessage} function which is called at the end of this one.

        @param command: A C{dict} for the command line of the event packet with
        keys 'method', 'resource' and 'protocol'.
        @param header: A C{dict} containing all headers from the event packet
        except the command line.
        @param body: C{str} containing the body of the event packet.
        """
        self.debug('checkNotifyMessage')
        #self.debug(header)
        #self.debug(body)
        # HOST header check
        if not header.has_key('host'):
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       'Notify: HOST header missing',
                       [command['path'], header['host']]
                       )

        # ACCEPT-LANGUAGE header shouldn't be here
        if header.has_key('accept-language'):
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       'Notify: ACCEPT-LANGUAGE header found, not allowed for unsubscribe message',
                       [header['host'], command['path']]
                       )

        # CONTENT-LENGTH header check
        if not header.has_key('content-length'):
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       'Notify: CONTENT-LENGTH header missing',
                       [header['host'], command['path']]
                       )
        elif int(header['content-length']) != len(body):
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       'Notify: Wrong CONTENT-LENGTH header value (%r, should be %r)' % (header['content-length'], len(body)),
                       [header['host'], command['path']]
                       )

        # CONTENT-TYPE header check
        if not header.has_key('content-type'):
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       'Notify: CONTENT-TYPE header missing',
                       [header['host'], command['path']]
                       )
        elif header['content-type'] != 'text/xml':
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       "Notify: Wrong CONTENT-TYPE header value (%r, should be 'text/xml')" % header['content-type'],
                       [header['host'], command['path']]
                       )

        # NT header check
        if not header.has_key('nt'):
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       'Notify: NT header missing',
                       [header['host'], command['path']]
                       )
        elif header['nt'] != 'upnp:event':
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       'Notify: Wrong NT header value (%r)' % header['nt'],
                       [header['host'], command['path']]
                       )

        # NTS header check
        if not header.has_key('nts'):
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       'Notify: NTS header missing',
                       [header['host'], command['path']]
                       )
        elif header['nts'] != 'upnp:propchange':
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       'Notify: Wrong NTS header value (%r)' % header['nt'],
                       [header['host'], command['path']]
                       )

        # SID header check
        if not header.has_key('sid'):
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       'Notify: SID header missing',
                       [header['host'], command['path']]
                       )
        elif not header['sid'].startswith('uuid:'):
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       "Notify: SID header has to start with 'uuid:' (%r)" % header['sid'],
                       [header['host'], command['path']]
                       )

        # SEQ header check
        if not header.has_key('seq'):
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       'Notify: SEQ header missing',
                       [header['host'], command['path']]
                       )
        else:
            service_ident = header['host'] + command['path']
            self.calcNextEventSeq(service_ident, header['sid'])
            if int(header['seq']) != self.notifySeq[service_ident][header['sid']]:
                louie.send(
                           'UPnT.event.notify_incorrect',
                           None,
                           "Notify: SEQ header has to be incremented starting with 0 (%r, should be %r)" % (header['seq'], self.notifySeq[service_ident]),
                           [header['host'], command['path']]
                           )

        self.validateMessage(body, header['host'], command['path'])

    def validateMessage(self, message, header_host, command_path):
        """
        The event message will be checked if its XML is well-formed.

        @param message: The body of the event packet.
        @param header_host: The host field of the header of the packet.
        @param command_path: The path of the event server.
        """

        self.info('validateMessage')

        doc = etree.parse(StringIO(message.strip()))

        if self._xmlschema is None:
            louie.send('UPnT.errorMessage', None, 'EventMessageError', 'No XML Schema to compare description against!')

        elif self._xmlschema.validate(doc) == 1:
            #louie.send('UPnT.infoMessage', None, 'Body of notify message valid! (%s)' % service_info)
            pass

        else:
            error = self._xmlschema.error_log.last_error
            self.warning(error)
            service_ident = header_host + command_path
            louie.send(
                       'UPnT.event.notify_incorrect',
                       None,
                       "Body of notify message not valid! (%s)" % service_ident,
                       [header_host, command_path]
                       )
            louie.send('UPnT.infoMessage', None, 'Body of notify message not valid! (%s%s)\nError: %s\n%s' %
                       (header_host, command_path, error, etree.tostring(doc, pretty_print=True)))
            self.debug('Message:\n%s' % message)


class ClientEventMessageChecks(log.Loggable):
    """
    This class contains event message checks for client devices, mainly the
    control point kind of devices. It runs as part of a virtual server device
    (like a MediaServer) and checks message sent by a client device.

    The main task is to listen to the C{UPnT.event.client_message_received}
    signal and process the data given by this signal.
    """

    logCategory = 'UPnT_ClientEventMessageChecks'
    """Log category for the client event checker."""

    def __init__(self):
        """
        Initialise the server event messsage checker and connect to the
        C{UPnT.event.client_message_received} signal.
        """
        louie.connect(self.checkEventMessage, 'UPnT.event.client_message_received', louie.Any, weak=False)
        self.debug('ClientEventMessageChecks initialized')

    def checkEventMessage(self, command, header, body):
        """
        Processing function that is called when a
        C{UPnT.event.client_message_received} signal is received.

        If the command in the event packet is known, the according checking
        function is called. Otherwise the erroneous packet will be displayed.

        @param command: A C{dict} for the command line of the event packet with
        keys 'method', 'resource' and 'protocol'.
        @param header: A C{dict} containing all headers from the event packet
        except the command line.
        @param body: C{str} containing the body of the event packet.
        """
        self.debug('checkEventMessage')
        #print command
        #print header
        #print body

        if command['method'].lower() == 'unsubscribe':
            self.checkUnsubscribeMessage(command, header, body)
        elif command['method'].lower() == 'subscribe' and header.has_key('sid'):
            self.checkRenewalMessage(command, header, body)
        elif command['method'].lower() == 'subscribe' and header.has_key('nt') and \
          header.has_key('callback'):
            self.checkSubscriptionMessage(command, header, body)
        else:
            self.debug(command)
            self.debug(header)
            self.debug(body)

    def checkSubscriptionMessage(self, command, header, body):
        """
        A subscription request was received and the headers of this packet are
        checked for validity.

        @param command: A C{dict} for the command line of the event packet with
        keys 'method', 'resource' and 'protocol'.
        @param header: A C{dict} containing all headers from the event packet
        except the command line.
        @param body: C{str} containing the body of the event packet which has
        to be empty.
        """
        self.debug('checkSubscriptionMessage')

        # HOST header check
        if not header.has_key('host'):
            louie.send(
                       'UPnT.event.subscription_incorrect',
                       None,
                       'Subscription: HOST header missing',
                       [header['host'], command['path']]
                       )

        # CALLBACK header check
        if not header.has_key('callback'):
            louie.send(
                       'UPnT.event.subscription_incorrect',
                       None,
                       'Subscription: CALLBACK header missing',
                       [header['host'], command['path']]
                       )

        # NT header check
        if not header.has_key('nt'):
            louie.send(
                       'UPnT.event.subscription_incorrect',
                       None,
                       'Subscription: NT header missing',
                       [header['host'], command['path']]
                       )
        elif header['nt'] != 'upnp:event':
            louie.send(
                       'UPnT.event.subscription_incorrect',
                       None,
                       'Subscription: Wrong NT header value (%r)' % header['nt'],
                       [header['host'], command['path']]
                       )

        # SID header shouldn't be here
        if header.has_key('sid'):
            louie.send(
                       'UPnT.event.subscription_incorrect',
                       None,
                       'Subscription: SID header found, not allowed for subscription message',
                       [header['host'], command['path']]
                       )

        # TIMEOUT header check
        if not header.has_key('timeout'):
            louie.send(
                       'UPnT.infoMessage',
                       None,
                       'A subscribe message should include a TIMEOUT header (%s)' % command['path']
                       )
        else:
            value = header['timeout'].split('-')
            if value[0].strip() == 'Second':
                try:
                    i = int(value[1].strip())
                except ValueError:
                    if value[1].strip() != 'infinite':
                        louie.send(
                                   'UPnT.event.subscription_incorrect',
                                   None,
                                   'Subscription: Wrong value for TIMEOUT header (%r)' % header['timeout'],
                                   [header['host'], command['path']]
                                   )
            else:
                louie.send(
                           'UPnT.event.subscription_incorrect',
                           None,
                           'Subscription: Wrong value for TIMEOUT header (%r)' % header['timeout'],
                           [header['host'], command['path']]
                           )

        # message shouldn't have body content
        if len(body) > 0:
            louie.send(
                       'UPnT.event.subscription_incorrect',
                       None,
                       'Subscription: Conctent found in body (%r)' % body,
                       [header['host'], command['path']]
                       )
        self.debug('checkSubscriptionMessage-Finished')

    def checkRenewalMessage(self, command, header, body):
        """
        A subscription renewal was received and the headers of this packet are
        checked for validity.

        @param command: A C{dict} for the command line of the event packet with
        keys 'method', 'resource' and 'protocol'.
        @param header: A C{dict} containing all headers from the event packet
        except the command line.
        @param body: C{str} containing the body of the event packet which has
        to be empty.
        """
        self.debug('checkRenewalMessage')
        # HOST header check
        if not header.has_key('host'):
            louie.send(
                       'UPnT.event.renewal_incorrect',
                       None,
                       'Renewal: HOST header missing',
                       [header['host'], command['path']]
                       )

        # CALLBACK header shouldn't be here
        if header.has_key('callback'):
            louie.send(
                       'UPnT.event.renewal_incorrect',
                       None,
                       'Renewal: CALLBACK header found, not allowed for renewal message',
                       [header['host'], command['path']]
                       )

        # NT header shouldn't be here
        if header.has_key('nt'):
            louie.send(
                       'UPnT.event.renewal_incorrect',
                       None,
                       'Renewal: NT header header found, not allowed for renewal message',
                       [header['host'], command['path']]
                       )

        # SID header check
        if not header.has_key('sid'):
            louie.send(
                       'UPnT.event.renewal_incorrect',
                       None,
                       'Renewal: SID header missing',
                       [header['host'], command['path']]
                       )
        elif not header['sid'].startswith('uuid:'):
            louie.send(
                       'UPnT.event.renewal_incorrect',
                       None,
                       "Renewal: SID header has to start with 'uuid:'",
                       [header['host'], command['path']]
                       )

        # TIMEOUT header check
        if not header.has_key('timeout'):
            louie.send(
                       'UPnT.infoMessage',
                       None,
                       'A subscribe message should include a TIMEOUT header (%s)' % command['path']
                       )
        else:
            value = header['timeout'].split('-')
            if value[0].strip() == 'Second':
                try:
                    i = int(value[1].strip())
                except ValueError:
                    if value[1].strip() != 'infinite':
                        louie.send(
                                   'UPnT.event.renewal_incorrect',
                                   None,
                                   'Renewal: Wrong value for TIMEOUT header (%r)' % header['timeout'],
                                   [header['host'], command['path']]
                                   )
            else:
                louie.send(
                           'UPnT.event.renewal_incorrect',
                           None,
                           'Renewal: Wrong value for TIMEOUT header (%r)' % header['timeout'],
                           [header['host'], command['path']]
                           )

        # message shouldn't have body content
        if len(body) > 0:
            louie.send(
                       'UPnT.event.renewal_incorrect',
                       None,
                       'Renewal: Conctent found in body (%r)' % body,
                       [header['host'], command['path']]
                       )

        self.debug('checkRenewalMessage-Finished')

    def checkUnsubscribeMessage(self, command, header, body):
        """
        A unsubscribe request was received and the headers of this packet are
        checked for validity.

        @param command: A C{dict} for the command line of the event packet with
        keys 'method', 'resource' and 'protocol'.
        @param header: A C{dict} containing all headers from the event packet
        except the command line.
        @param body: C{str} containing the body of the event packet which has
        to be empty.
        """
        self.debug('checkUnsubscribeMessage')
        # HOST header check
        if not header.has_key('host'):
            louie.send(
                       'UPnT.event.unsubscribe_incorrect',
                       None,
                       'Unsubscribe: HOST header missing',
                       [header['host'], command['path']]
                       )

        # CALLBACK header shouldn't be here
        if header.has_key('callback'):
            louie.send(
                       'UPnT.event.unsubscribe_incorrect',
                       None,
                       'Unsubscribe: CALLBACK header found, not allowed for unsubscribe message',
                       [header['host'], command['path']]
                       )

        # NT header shouldn't be here
        if header.has_key('nt'):
            louie.send(
                       'UPnT.event.unsubscribe_incorrect',
                       None,
                       'Unsubscribe: NT header header found, not allowed for unsubscribe message',
                       [header['host'], command['path']]
                       )

        # SID header check
        if not header.has_key('sid'):
            louie.send(
                       'UPnT.event.unsubscribe_incorrect',
                       None,
                       'Unsubscribe: SID header missing',
                       [header['host'], command['path']]
                       )
        elif not header['sid'].startswith('uuid:'):
            louie.send(
                       'UPnT.event.unsubscribe_incorrect',
                       None,
                       "Unsubscribe: SID header has to start with 'uuid:'",
                       [header['host'], command['path']]
                       )

        # TIMEOUT header shouldn't be here
        if header.has_key('timeout'):
            louie.send(
                       'UPnT.event.unsubscribe_incorrect',
                       None,
                       'Unsubscribe: TIMEOUT header header found, not allowed for unsubscribe message',
                       [header['host'], command['path']]
                       )
        # message shouldn't have body content
        if len(body) > 0:
            louie.send(
                       'UPnT.event.unsubscribe_incorrect',
                       None,
                       'Renewal: Conctent found in body (%r)' % body,
                       [header['host'], command['path']]
                       )

        self.debug('checkUnsubscribeMessage-Finished')

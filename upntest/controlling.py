"""
Module that holds the classes for checking control messages and responses
sent by client or server devices.

@license: Licensed under the MIT license
    http://opensource.org/licenses/mit-license.php
@author: Michael Weinrich <testsuite@michael-weinrich.de>
@copyright: Copyright 2007, Michael Weinrich
"""

# external includes
import louie
from lxml import etree
import os
import time
from twisted.internet import reactor, defer
from twisted.python.filepath import FilePath
# Coherence includes
from coherence import log
# local includes
from upntest.soap_proxy import SOAPProxy


class ServerControl(log.Loggable):
    """
    This class contains methods to invoke all actions that a service offers and
    to check the responses for validity.

    The actions that are invoked are read from the service template that is
    selected based on the service type. The actions listed in there can be
    customized or removed using an additional file containing the according
    instructions (see usage documentation).
    """

    logCategory = 'UPnT_ServerControl'
    """Log category for the server control checker."""

    def __init__(self, parent, config, service_type, service_version, uuid):
        """
        Initailise the server control messages checker.

        @param parent: "Service that is to be tested.
        @param config: A C{dict} containing the configuration data from the
        config file.
        @param service_type: Type of service that is to be tested.
        @param service_version: Version of service type.
        @param uuid: UUID of the service to be tested (C{str}).
        """

        self.parentService = parent
        """Service that is tested."""
        self._testcasesXml = None
        """XML file containing the service description with all state variables
        and actions."""
        self._uuidXml = None
        """Device specific service description that is merged with the general
        one in L{_testcasesXml}."""

        self._ready = True
        """A C{bool} indicating that the device initialisation was successful."""
        if service_type != "" and \
           service_version != "" and \
           uuid != "":
            test_file_dir = config.get('test_file_dir', FilePath(__file__).parent().parent().child('testcases').path)

            if not test_file_dir.endswith('/'):
                test_file_dir += '/'

            if test_file_dir is not None:
                try:
                    self._testcasesXml = \
                        etree.parse(test_file_dir + 'common/' +
                                    service_type + service_version + '.SyntaxTests.xml')
                    try:
                        self._uuidXml = \
                            etree.parse(test_file_dir + 'uuid/' +
                                        service_type + service_version + '.' + uuid + '.SyntaxTests.xml')
                    except IOError, error:
                        self.info('Specific file for %s:%s on uuid:%s not found' % (service_type, service_version, uuid))
                        self.info(error)
                        #self._ready = False
                        self._uuidXml = None
                    except etree.XMLSyntaxError, error:
                        self.info('Specific file for %s:%s on uuid:%s cannot be parsed' % (service_type, service_version, uuid))
                        self.info(error)
                        #self._ready = False
                        self._uuidXml = None
                except IOError, error:
                    self.warning('Common file for %s:%s not found' % (service_type, service_version))
                    self.warning(error)
                    self._ready = False
                except etree.XMLSyntaxError, error:
                    self.warning('Common file for %s:%s cannot be parsed' % (service_type, service_version))
                    self.warning(error)
                    self._ready = False
        if not self._ready:
            return

        self.loop_tests = False
        """A C{bool} indicating whether the tests should be run repeatedly until
        this variable is set to C{False} (which is the default)."""

        self._testcases = {}
        """A C{dict} that is used to store all the data neccessary to invoke an
        action on the remote device."""
        self.parseTestcases()

    def parseTestcases(self):
        """
        Parse files with test cases and create list for testing.

        Parses the file with common information about the testcases. Afterwards
        filter that input using the file with UUID specific information.
        """
        self.debug('parseTestcases')

        # get all testcases from the common file
        testcases = self._testcasesXml.xpath('/ServiceControlSyntaxTestCases/TestCaseList/TestCase')
        for testcase in testcases:
            id = testcase.findtext('Id')
            self._testcases[id] = {}
            self._testcases[id]['Category'] = testcase.findtext('Category')
            self._testcases[id]['ActionName'] = testcase.findtext('ActionName')
            in_args = testcase.findall('./InArgs/*')
            if len(in_args) > 0:
                self._testcases[id]['InArgs'] = {}
                for argument in in_args:
                    self._testcases[id]['InArgs'][argument.tag] = argument.text
            self._testcases[id]['ExpectedReturnCode'] = testcase.findtext('ExpectedReturnCode')

        #self.debug(self._testcases)

        # read the device specific file and make according changes
        #self.debug(etree.tostring(self._uuidXml, pretty_print=True))
        if self._uuidXml is not None:
            specific_changes = self._uuidXml.xpath('/ServiceControlSyntaxTestCases/TestCaseList/TestCase')
            for change in specific_changes:
                id = change.findtext('Id')
                if self._testcases.has_key(id):
                    self.debug('change.get = ' + change.get('delete', 'no'))
                    if change.get('delete', 'no') == 'yes':
                        del self._testcases[id]
                    else:
                        category = change.findtext('Category')
                        action_name = change.findtext('ActionName')
                        in_args = change.findall('./InArgs/*')
                        expected_return_code = change.findtext('ExpectedReturnCode')

                        bool1 = self._testcases[id].has_key('InArgs') and len(in_args) == len(self._testcases[id]['InArgs'])
                        bool2 = not self._testcases[id].has_key('InArgs') and len(in_args) == 0
                        if category != "" and action_name != "" and expected_return_code != "" and \
                           ((bool1 and not bool2) or (not bool1 and bool2)):
                            self._testcases[id]['Category'] = category
                            self._testcases[id]['ActionName'] = action_name
                            self._testcases[id]['ExpectedReturnCode'] = expected_return_code
                            if bool1:
                                for argument in in_args:
                                    self._testcases[id]['InArgs'][argument.tag] = argument.text

    def startTesting(self, loop_tests=False):
        """
        Set all variables and start the test asynchronusly.

        @param loop_tests: If C{True}, the test loop starts again at the beginning
        of the method list.

        @return: A C{Deferred} which is activated when the test run is
        finished. If L{_ready} is still C{False}, an already activated C{Deferred}
        is returned.
        """
        self.debug('startTesting')
        if not self._ready:
            self.debug('Device not ready')
            return defer.succeed(False)

        self.loop_tests = loop_tests
        #self.test_deferred = defer.Deferred()
        reactor.callLater(1, self.doTesting)
        #return self.test_deferred

    def doTesting(self):
        """
        Execution of the actual test loop.

        Go through all testcases and send a control message for each test case.
        If L{loop_tests} is C{True}, repeat the tests until it is set to C{False}.
        """
        #print 'doTesting...'

        self.debug('doTesting')
        is_looped = True
        test_result = True
        url = self.parentService._urlBase + self.parentService.controlUrl
        namespace = self.parentService.schema + ':' + self.parentService.serviceType + ':' + self.parentService.serviceVersion
        instance_id = 0
        while is_looped:
            for testcase_id, testcase in self._testcases.items():
                action = "%s#%s" % (namespace, testcase['ActionName'])
                if testcase.has_key('InArgs') and testcase['InArgs'].has_key('InstanceID'):
                    instance_id = testcase['InArgs']['InstanceID']
                callClient = SOAPProxy(url, namespace=("u", namespace), soapaction=action)
                # actual call
                in_args = {}
                if testcase.has_key('InArgs'):
                    in_args = testcase['InArgs']
                d = callClient.callRemote(testcase['ActionName'], in_args)
                d.addCallback(self.callSucceeded, action, instance_id)
                d.addErrback(self.callFailed, action, url, in_args, testcase['ActionName'])

            is_looped = self.loop_tests
        #self.test_deferred.callback(test_result)

    def stopTesting(self):
        """
        Set L{loop_tests} to C{False} and stop the test loop after the current
        cycle.
        """
        self.loop_tests = False

    def callFailed(self, failure, action, url, args, action_name):
        """
        Callback for an unsuccessful action invocation.

        @param failure: The failure that caused the error.
        @param action: Name of the action that failed including service type
        (C{str}).
        @param url: URL of the event server (C{str}).
        @param args: C{dict} containing all IN arguments of the action.
        @param action_name: Name of the action (C{str}).
        """

        self.warning("error: invoking %s on %s with %r" % (action,
                                                          url,
                                                          args))
        self.info(failure)
        action_list = self.parentService._actionList
        if action_list.has_key(action_name):
            if not action_list[action_name]['Optional']:
                louie.send('UPnT.infoMessage', None,
                           "Required action %s couldn't be invoked on %s with %r" % (action,
                                                                                     url,
                                                                                     args))
        #return failure

    def callSucceeded(self, results, action, instance_id):
        """
        Callback for a successful action invocation.

        @param results: The results of the action invocation.
        @param action: Name of the action including service type (C{str}).
        @param instance_id: Instance ID that was used during the invocation
        (C{int}).
        """

        louie.send('UPnT.infoMessage', None,
                   "\nok: call %s (instance %d) returned" % (action, instance_id))
        if len(results) > 0:
            for out_arg, out_val in results.items():
                louie.send('UPnT.infoMessage', None,
                           "%s: %s" % (out_arg, out_val))
        else:
            louie.send('UPnT.infoMessage', None,
                       'no return values' % (action, instance_id))


class ClientControl(log.Loggable):
    """
    Device to check is a control point so we can only perform passive checks on
    the messages sent by the device.
    """

    logCategory = 'UPnT_ClientControl'
    """Log category for the client control checker."""

    serviceActions = {}
    """C{dict} conatining all the actions that are found in all the available
    service templates. This is a class variable so that the templates are
    available to all instances once the first instance parsed all templates."""

    def __init__(self, templates_dir):
        """
        Read all avilable service templates and store all actions found in
        L{serviceActions}.

        @param templates_dir: Path to the directory where the service and device
        templates are located (C{str}).
        """

        if not templates_dir.endswith('/'):
            templates_dir += '/'

        if len(self.serviceActions) == 0:
            template_files = [template_file for template_file in os.listdir(templates_dir + 'service/') if not template_file.startswith('.')]

            ns = {'upnp': 'urn:schemas-upnp-org:service-1-0'}
            for template_file in template_files:

                service_template = etree.parse(templates_dir + 'service/' + template_file)

                # get names of actions and if they're required or not
                template_action_list = service_template.xpath('actionList/action')

                tpl_actions = {}
                for action_node in template_action_list:
                    tpl_name = action_node.xpath('name')[0].text.strip()
                    tpl_arguments = {}
                    for argument in action_node.xpath('argumentList/argument'):
                        tpl_arg_name = argument.xpath('name')[0].text.strip()
                        #self.debug('ActionName %s, ArgumentName %s' % (tpl_name, tpl_arg_name))
                        tpl_arg_dir = argument.xpath('direction')[0].text.strip()
                        tpl_arg_state_var = argument.xpath('relatedStateVariable')[0].text.strip()
                        tpl_arguments[tpl_arg_name] = {'direction': tpl_arg_dir, 'rel_state_var': tpl_arg_state_var}
                    tpl_actions[tpl_name] = {}
                    tpl_actions[tpl_name]['Arguments'] = tpl_arguments
                    tpl_actions[tpl_name]['Optional'] = (len(action_node.xpath('Optional')) == 0)

                self.serviceActions[template_file[:-4]] = tpl_actions

        louie.connect(self.checkControlMessage, 'UPnT.control_message', louie.Any, weak=False)

    def checkControlMessage(self, command, header, body, remotehost):
        """
        Check control message received from a Control Point.

        @param command: A C{dict} for the command line of the event packet with
        keys 'method', 'resource' and 'protocol'.
        @param header: A C{dict} containing all headers from the event packet
        except the command line.
        @param body: C{str} containing the body of the event packet.

        @return: C{True} if the packet was valid or C{False} if it was invalid.
        """

        # HTTP command check
        if command['method'] != 'POST' and  command['method'] != 'M-POST':
            louie.send('UPnT.infoMessage',
                       None,
                       'Control: Wrong method (has to be POST or M-POST, %r --> %s/%s)' % (remotehost, header['host'], command['path'])
                       )
            return False

        # HOST header check
        if not header.has_key('host'):
            louie.send('UPnT.infoMessage',
                       None,
                       'Control: HOST header missing (%r --> %s/%s)' % (remotehost, header['host'], command['path'])
                       )
            return False

        # CONTENT-LENGTH header check
        if not header.has_key('content-length'):
            louie.send('UPnT.infoMessage',
                       None,
                       'Control: CONTENT-LENGTH header missing (%r --> %s/%s)' % (remotehost, header['host'], command['path'])
                       )
            return False
        elif int(header['content-length']) != len(body):
            louie.send('UPnT.infoMessage',
                       None,
                       'Control: Wrong CONTENT-LENGTH header value (%r, should be %r, %r --> %s/%s)' % (header['content-length'], len(body), remotehost, header['host'], command['path'])
                       )
            return False

        # CONTENT-TYPE header check
        if not header.has_key('content-type'):
            louie.send('UPnT.infoMessage',
                       None,
                       'Control: CONTENT-TYPE header missing (%r --> %s/%s)' % (remotehost, header['host'], command['path'])
                       )
            return False
        elif not header['content-type'].startswith('text/xml'):
            louie.send('UPnT.infoMessage',
                       None,
                       'Control: Wrong CONTENT-TYPE header value (%r, should be "text/xml") (%r --> %s/%s)' % (header['content-type'], remotehost, header['host'], command['path'])
                       )
            return False
        elif header['content-type'].lower().count('charset="utf-8"') == 0:
            louie.send('UPnT.infoMessage', None, 'You should include charset="utf-8" in the CONTENT-TYPE header of the control commands of %r.' % remotehost)

        # for POST just SOAPACTION header, for M-POST MAN and xx-SOAPACTION
        soapaction = ''
        if command['method'] == 'POST':
            if not header.has_key('soapaction'):
                louie.send('UPnT.infoMessage',
                           None,
                           'Control: SOAPACTION header missing (%r --> %s/%s)' % (remotehost, header['host'], command['path'])
                           )
                return False
            else:
                soapaction = header['soapaction']
        else:
            if not header.has_key('man'):
                louie.send('UPnT.infoMessage',
                           None,
                           'Control: MAN header missing, because packet uses M-POST (%r --> %s/%s)' % (remotehost, header['host'], command['path'])
                           )
                return False
            man_header_val = header['man']
            man_ns = man_header_val[man_header_val.find('=')+1:].lower()
            if not header.has_key(man_ns + '-soapaction'):
                louie.send('UPnT.infoMessage',
                           None,
                           'Control: %s-SOAPACTION header missing, because packet uses M-POST (%r --> %s/%s)' % (man_ns, remotehost, header['host'], command['path'])
                           )
                return False
            soapaction = header[man_ns + '-soapaction']

        if len(soapaction) == 0:
            louie.send('UPnT.infoMessage',
                       None,
                       'Control: SOAPACTION empty (%r --> %s/%s)' % (remotehost, header['host'], command['path'])
                       )
            return False

        actionName = soapaction[soapaction.find('#')+1:].rstrip('"')
        action_parts = soapaction[:soapaction.find('#')].split(':')
        serviceName = action_parts[3] + action_parts[4]

        if not self.serviceActions.has_key(serviceName):
            louie.send('UPnT.infoMessage',
                       None,
                       'Control: Unknown service name %s (%r --> %s/%s)' % (serviceName, remotehost, header['host'], command['path'])
                       )
            return False

        if not self.serviceActions[serviceName].has_key(actionName):
            louie.send('UPnT.infoMessage',
                       None,
                       'Control: SOAPACTION %s not defined for device type %s (%r --> %s/%s)' % (actionName, serviceName, remotehost, header['host'], command['path'])
                       )
            return False

        return True
        self.debug('Control call ok')

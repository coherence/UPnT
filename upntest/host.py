"""
Module that holds the Host class of the UPnP Test Tool.

@license: Licensed under the MIT license 
    http://opensource.org/licenses/mit-license.php
@author: Michael Weinrich <testsuite@michael-weinrich.de>
@copyright: Copyright 2007, Michael Weinrich
"""

# external includes
import urllib2
import string
import louie
from time import time
from twisted.internet import reactor, defer
# Coherence includes
from coherence.upnp.core import utils
from coherence import log
# local includes
from upntest.device import ServerDevice, ClientDevice
from upntest.service import Service


class Host(log.Loggable):
    """
    Helper class to keep track of hosts and their behaviour. This is the first 
    item in the network hierarchy and contains a list of all devices and 
    services available on this host.
    """

    logCategory = 'UPnT_Host'
    """Debug category for the host class."""

    def __init__(self, ip, config, coherence):
        """
        Initialise the host.
        
        This also initialises a L{ClientDevice} object for tracking of 
        ControlPoint messages.
        
        @param ip: IP of this host (C{str})
        @param config: A C{dict} containing the configuration data from the 
        config file.
        @param coherence: The main C{Coherence} 
        instance used to communicate with the UPnP devices.
        """

        self._config = config
        """Stores the given config options to pass it on to other objects who 
        need it."""

        self._ip = ip
        """The IP of the host represented by this object."""

        self._serverDevices = {}
        """A C{dict} containing all server devices that run on this host. These 
        are devices like MediaServers or MediaRenderers."""

        self._clientDevice = ClientDevice(config)
        """An instance of L{ClientDevice} that tracks all messages sent by a 
        Control Point."""

        self._services = {}
        """A C{dict} containing all services that run on this host."""

        self._createDevTreeFunc = None
        """An object which provides C{IDelayedCall} for 
        L{createAndCheckDeviceTree} which is called 3 seconds after the last 
        announce message for this host."""

        self._coherence = coherence
        """The C{Coherence} instance that was passed in the C{coherence} 
        parameter."""

    def getIp(self):
        """
        Return IP of this host.
        
        @return: IP of this host (C{str})
        """
        return self._ip
    ip = property(getIp)
    """IP of this host."""

    def dispatchDiscoveryPacketData(self, data):
        """
        Forward packet data to the right check method.
        
        Based on the header this method decides which check function should 
        process the data. This is:
          - L{checkDiscoveryPacket} for a C{M-SEARCH} request
          - L{checkNotificationPacket} for a C{NOTIFY} packet
          - L{checkDiscoveryAnswerPacket} a paket that was send as an answer to 
            a C{M-SEARCH} request
        
        @param data: A C{string} containing the packet data.
        """
        self.debug('dispatchDiscoveryPacketData')
        [header, payload] = data.split('\r\n\r\n', 1)
        #self.showPacketData(None, header, payload, '', None)
        if data.lower().startswith('m-search'):
            self.debug('Packet is of type M_SEARCH')
            d = self.checkDiscoveryPacket(header, payload)
            if d is not None:
                #d.addCallback(self.updateDeviceTree, header, d)
                #d.addErrback(self.showPacketData, header, payload, '', d)
                #louie.send('UPnT.ending_deferred', None, d)
                pass
            else:
                self.showPacketData(None, header, payload, '', None)
        elif data.lower().startswith('notify'):
            self.debug('Packet is of type NOTIFY')
            d = self.checkNotificationPacket(header, payload)
            if d is not None:
                d.addCallback(self.updateDeviceTree, header, d)
                d.addErrback(self.showPacketData, header, payload, 'location', d)
            else:
                self.showPacketData(None, header, payload, '', None)
        elif data.lower().startswith('http'):
            self.debug('Packet is of type HTTP (M-SEARCH response)')
            d = self.checkDiscoveryAnswerPacket(header, payload)
            if d is not None:
                d.addCallback(self.updateDeviceTree, header, d)
                d.addErrback(self.showPacketData, header, payload, 'location', d)
            else:
                self.showPacketData(None, header, payload, '', None)

    def formatHeader(self, header):
        """
        Split header in command line and header lines.
        
        @param header: A L{str} containing the header of the SSDP packet.
        @return: A L{list} containing a C{dict} for the command line with keys 
        'method', 'resource' and 'protocol' and a C{dict} with keys and values 
        from the other headers. All keys are lowercase.
        """

        #print header
        #print ' '
        lines = header.split('\r\n')
        #split first line to get the command, requested resource and protocol version
        cmd = string.split(lines[0], ' ')

        if len(cmd) < 3:
            command = {'method': '', 'resource': '', 'protocol': ''}
        else:
            command = {'method': cmd[0].lower(),
                       'resource': cmd[1].lower(),
                       'protocol': cmd[2].lower()}

        #format all lines but the first and transform it into a dictionary
        lines = map(lambda x: x.replace(': ', ':', 1), lines[1:])
        lines = filter(lambda x: len(x) > 0, lines)
        headers = [string.split(x, ':', 1) for x in lines]
        headers = dict(map(lambda x: (x[0].lower(), x[1]), headers))

        return [command, headers]

    def showPacketData(self, failure, header, payload, error, d):
        """
        Print packet data.
        
        This method is called when a check method returned an error while 
        checking a packet. It prints failure, header and payload to stdout. If 
        the C{Deferred} in parameter C{d} is defined the L{UPnT<base.UPnT>} 
        class is informed about the end of the processing chain.
        
        @param failure: C{Exception} that caused the error.
        @param header: C{str} containing all headers of the erroneous packet.
        @param payload: C{str} containing the body of the erroneous packet.
        @param error: C{str} describing the error that happened.
        @param d: C{Deferred} that was used to process the packet.
        """

        self.warning(str(failure))
        command, headers = self.formatHeader(header)
        if error == 'location':
            louie.send('UPnT.errorMessage',
                       None,
                       'HostValidationError (%s)' % self.ip,
                       'LOCATION not reachable (%s)\n\n%s' %
                       (headers['location'], header))
            self.warning('LOCATION not reachable (%s)' % headers['location'])

        self._discovery_packets_valid = False
        self.warning('\nErroneous datagram received from host %s' % self.ip)
        self.warning('Datagram header: %s' % header)
        self.warning('Datagram payload: %s' % payload)
        #if d is not None:
        #    louie.send('UPnT.ending_deferred', None, d)

    def updateDeviceTree(self, result, header, d):
        """
        Register a new device or delete an existing device based on the command 
        and the UUID from the SSDP packet. At the end tell L{UPnT<base.UPnT>} 
        class that C{Deferred} L{d} is at the end of the processing chain.
        
        @param result: Return data from the Deferred (not used).
        @param header: C{str} containing all headers of the SSDP packet.
        @param d: C{Deferred} that was used to process the packet.
        """

        command, headers = self.formatHeader(header)

        usn_split = headers['usn'].split('::')
        uuid = usn_split[0][5:]
        upnp_type = ''
        if len(usn_split) > 1:
            upnp_type_split = usn_split[1].split(':')
            if len(upnp_type_split) > 2:
                upnp_type = upnp_type_split[2]
            else:
                upnp_type = upnp_type_split[1]

        self.debug('Processing USN %s from %s' % (headers['usn'], self.ip))
        self.debug('Processing UUID %s from %s' % (uuid, self.ip))

        if headers.has_key('nts'):
            action = headers['nts']
        else:
            # packet was an M-SEARCH response therefore it has no NTS header
            action = 'ssdp:alive'

        if action == 'ssdp:alive':

            
            if upnp_type == 'service':
                if self._services.has_key(headers['usn']):
                    self._services[headers['usn']].update(headers)
                else:
                    self._services[headers['usn']] = Service(headers,
                                                             self._config)

            else:
                if self._serverDevices.has_key(uuid):
                    self._serverDevices[uuid].update(headers,
                                                     self._serverDevices)
                else:
                    self._serverDevices[uuid] = ServerDevice(headers,
                                                             self._config)
                    self._serverDevices[uuid].host = self

            if self._createDevTreeFunc is None:
                self._createDevTreeFunc = \
                    reactor.callLater(3, self.createAndCheckDeviceTree)
            else:
                self._createDevTreeFunc.reset(3)

        elif action == 'ssdp:byebye':

            if upnp_type == 'device':
                if self._serverDevices.has_key(uuid):
                    del self._serverDevices[uuid]
            else:
                if self._serverDevices.has_key(uuid):
                    self._serverDevices[uuid].removeService(headers)
                if self._services.has_key(headers['usn']):
                    del self._services[headers['usn']]

        else:
            louie.send('UPnT.infoMessage',
                       None,
                       'Wrong action. Something went really wrong here.')
        #louie.send('UPnT.ending_deferred', None, d)

    def showDeviceTree(self, result, device, level=1):
        """
        Show a simple device tree based on the L{ServerDevice} given in 
        C{device} parameter.
        
        @param result: Return data from the Deferred (not used).
        @param device: The L{ServerDevice} whose subdevices and services should 
        be shown.
        @param level: The indentation level of the current call.
        """
        if not self._config.get('GUI', False):
            if level == 1:
                print
            if level > 1:
                print (level - 1) * '  ' + '+-D: ' + device.deviceType + ':' + \
                    device.deviceVersion
            else:
                print level * '  ' + 'D: ' + device.deviceType + ':' + \
                    device.deviceVersion
            #self.debug(device.services)
            for _, service in device.services.items():
                print level * '  ' + '+-S: ' + service.serviceType + ':' + \
                    service.serviceVersion
            #self.debug(device.subDevices)
            for _, sub_device in device.subDevices.items():
                self.showDeviceTree(result, sub_device, level + 1)

        # GUI update signal
        #print 'Sending GUI message'
        louie.send('UPnT.host_discovery_end', None, device)

    def createAndCheckDeviceTree(self):
        """
        Tell the root device to create its subdevice tree.
        
        Loop through all known devices and if a root device is found, the 
        L{checkDescriptions<ServerDevice.checkDescriptions>} method of this 
        device is called to check the device and service descriptions. When the 
        checks are finished L{showDeviceTree} is called.
        """

        self._createDevTreeFunc = None

        for uuid, device in self._serverDevices.items():
            if device.isRootDevice():
                d = device.checkDescriptions(self._serverDevices, self._services)
                d.addCallback(self.showDeviceTree, device)

    def checkNotificationPacket(self, header, payload):
        """
        Check common content of a notification packet for errors.
        
        The headers that are checked are:
          - the command line
          - HOST
          - NT
          - NTS
          - USN
        
        @param header: C{str} containing all headers.
        @param payload: C{str} containing the body of the SSDP packet that has 
        to be empty.
        @return: A C{Deferred} when everything was ok. The callbacks of the 
        Deferred are used for the next processing steps. Returns None if an 
        error occured during checking.
        """

        self.info('Processing NOTIFY packet from %s' % self.ip)

        self._last_announcement = time()

        command, headers = self.formatHeader(header)

        #check if command line is complete
        if not(len(command['method']) > 0 \
               and len(command['resource']) > 0 \
               and len(command['protocol']) > 0):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Validation of method failed')
            self.debug('Validation of method failed')
            return None

        #method must be NOTIFY
        if not (command['method'] == 'notify' and \
                command['resource'] == '*'):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Validation of method failed')
            self.debug('Validation of method failed')
            return None

        #following headers are mandatory for both packet types (alive and byebye)
        if not(headers.has_key('host') \
               and headers.has_key('nt') \
               and headers.has_key('nts') \
               and headers.has_key('usn')):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Mandatory header missing')
            self.debug('Mandatory header missing')
            return None

        #HOST must be 239.255.255.250, port can be omitted
        if not headers['host'].startswith('239.255.255.250'):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Wrong HOST header')
            self.debug('Wrong HOST header')
            return None

        #NT header
        nt_headers = headers['nt'].split(':')
        nt_headers = filter(lambda x: len(x) > 0, nt_headers)
        if not (len(nt_headers) == 2 \
                or len(nt_headers) == 5):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Wrong NT header')
            self.debug('Wrong NT header')
            return None

        if len(nt_headers) == 2 and \
            (not (headers['nt'].startswith('upnp:rootdevice') or \
                  headers['nt'].startswith('uuid:'))):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Wrong NT header')
            self.debug('Wrong NT header')
            return None
        #further testing possible?

        #USN header
        if not headers['usn'].startswith('uuid:'):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Wrong USN header')
            self.debug('Wrong USN header')
            return None

        usn_headers = headers['usn'].split('::')
        if len(usn_headers) == 2:
            if not (usn_headers[1] == 'upnp:rootdevice' or \
                    (usn_headers[1].startswith('urn:') and \
                     len(usn_headers[1].split(':')) == 5)):
                louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Wrong USN header')
                self.debug('Wrong USN header')
                return None

        #check if 2nd part of USN header is equal to NT header if NT header is not the UUID
        if headers['nt'].startswith('uuid:'):
            if headers['nt'] != headers['usn']:
                self.debug('NT header doesn\'t match USN header')
                return None
        else:
            usn_split = headers['usn'].split('::')
            if headers['nt'] != usn_split[1]:
                self.debug('NT header doesn\'t correspond USN header')
                return None

        #SSDP only needs the header data
        if len(payload) > 0:
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Packet has payload')
            self.debug('Packet has payload')
            return None

        #NTS has to be ssdp:alive or ssdp:byebye
        if headers['nts'] == 'ssdp:alive':
            return self.checkAlivePacket(headers)
        elif headers['nts'] == 'ssdp:byebye':
            self.debug('ssdp:byebye received')
            d = defer.succeed(None)
            #louie.send('UPnT.running_deferred', None, d)
            return d
        else:
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Wrong NTS header')
            self.debug('Wrong NTS header')
            return None

    def checkAlivePacket(self, header):
        """
        Check special content of an alive notification packet for errors. The 
        checked headers are:
          - CACHE-CONTROL max-age
          - LOCATION which is also checked for reachability.
          - SERVER

        @param header: A C{dict} containing all headers from the SSDP packet 
        except the command line.
        @return: A C{Deferred} when an attempt to download the description was 
        made. Otherwise None is returned when an error occured.
        """

        self.info('Processing NOTIFY ssdp:alive packet from %s' % self.ip)

        if not header.has_key('cache-control'):
            louie.send('UPnT.errorMessage',
                       None,
                       'HostValidationError (%s)' % self.ip,
                       'CACHE-CONTROL MAX-AGE missing')
            self.debug('Mandatory header missing')
            return None

        if not header.has_key('location'):
            louie.send('UPnT.errorMessage',
                       None,
                       'HostValidationError (%s)' % self.ip,
                       'LOCATION max-age missing')
            self.debug('Mandatory header missing')
            return None

        if not header.has_key('server'):
            louie.send('UPnT.errorMessage',
                       None,
                       'HostValidationError (%s)' % self.ip,
                       'SERVER missing')
            self.debug('Mandatory header missing')
            return None

        #cache-control must have max-age directive, should be >= 1800 seconds
        cache_control = header['cache-control'].split('=')
        if len(cache_control) != 2 \
              or cache_control[0].strip() != 'max-age' \
              or not cache_control[1].strip().isdigit():
            louie.send('UPnT.errorMessage',
                       None,
                       'HostValidationError (%s)' % self.ip,
                       'cache-control max-age missing')
            self.debug('cache-control max-age missing')
            return None

        #retrieve UUID to ease tracking of faulty device
        usn_split = header['usn'].split('::')
        uuid = usn_split[0][5:]

        #give hint about to low value for max-age
        if int(cache_control[1].strip()) < 1800:
            self._ssdp_max_age_to_low = True
            louie.send('UPnT.infoMessage',
                       None,
                       'You should check the max-age (%s) for your device %s/%s. 1800 seconds is recommended.' % (cache_control[1].strip(), self.ip, uuid))

        #location must be a valid URL
        if not len(header['location']):
            louie.send('UPnT.errorMessage',
                       None,
                       'HostValidationError (%s)' % self.ip,
                       'LOCATION missing')
            self.debug('LOCATION missing')
            return None

        # reachability is checked later on

        #Server has to have UPnP/1.0 in the string
        if header['server'].find('UPnP/1.0') == -1:
            louie.send('UPnT.errorMessage',
                       None,
                       'HostValidationError (%s)' % self.ip,
                       'UPnP/1.0 in SERVER header missing')
            self.debug('UPnP/1.0 in SERVER header missing')
            return None

        self.info('Trying to fetch description...')
        d = None
        if self._config.get('use_HEAD_request', False):
            d = utils.getPage(header['location'], method='HEAD')
        else:
            d = utils.getPage(header['location'])
        #louie.send('UPnT.running_deferred', None, d)
        return d

    def checkDiscoveryPacket(self, header, payload):
        """
        Check content of a discovery packet for errors. The checked headers are: 
          - HOST
          - MAN
          - MX
          - ST
        
        @param header: A C{dict} containing all headers from the SSDP packet 
        except the command line.
        @param payload: C{str} containing the body of the SSDP packet that has  
        to be empty.
        @return: None if one of the checks failed or an already succeeded 
        C{Deferred} that calls attached callbacks immediately.
        """

        self.info('Processing M-SEARCH request from %s' % self.ip)

        command, headers = self.formatHeader(header)

        #check if command line is complete
        if not(len(command['method']) > 0 \
               and len(command['resource']) > 0 \
               and len(command['protocol']) > 0):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'method not valid')
            self.debug('method not valid')
            return None

        #method must be M-SEARCH *
        if command['method'] != 'm-search' \
                or command['resource'] != '*':
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'method not valid')
            self.debug('method not valid')
            return None

        #all headers are mandatory
        if not(headers.has_key('host') \
               and headers.has_key('man') \
               and headers.has_key('mx') \
               and headers.has_key('st')):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Mandatory header missing')
            self.debug('Mandatory header missing')
            return None

        #HOST must be 239.255.255.250, port can be omitted
        if not headers['host'].startswith('239.255.255.250'):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Wrong HOST header')
            self.debug('Wrong HOST')
            return None

        #MAN header must be ssdp:discover
        if not headers['man'] == '"ssdp:discover"':
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Wrong MAN header')
            self.debug('Wrong MAN header')
            return None

        #MX header must be a number, should be between 1 and 120
        if not headers['mx'].strip().isdigit():
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Invalid MX header')
            self.debug('Wrong MX header')
            return None

        if int(headers['mx']) > 120:
            self._mx_value_to_high = True
            louie.send('UPnT.infoMessage', None, 'You should check the MX value (%s) from %s. It is recommended to be lower than or equal to 120 seconds.' % (headers['mx']))

        #ST is similar to NT
        st_headers = headers['st'].split(':')
        st_headers = filter(lambda x: len(x) > 0, st_headers)
        if not (len(st_headers) == 2 \
                or len(st_headers) == 5):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Wrong ST header')
            self.debug('Wrong ST header')
            return None

        if len(st_headers) == 2 and \
            (not (headers['st'] == 'ssdp:all' or \
                  headers['st'] == 'upnp:rootdevice' or \
                  headers['st'].startswith('uuid:'))):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Wrong ST header')
            self.debug('Wrong ST header')
            return None
        #further testing possible?

        #SSDP only needs the header data
        if len(payload) > 0:
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Packet has payload')
            self.debug('Packet has payload')
            return None

        d = defer.succeed(None)
        #louie.send('UPnT.running_deferred', None, d)
        return d

    def checkDiscoveryAnswerPacket(self, header, payload):
        """checks the answer of a discovery packet for errors and returns 
        C{True} if everything looks ok."""

        self.info('Processing M-SEARCH response from %s' % self.ip)

        command, headers = self.formatHeader(header)

        #check if command line is complete
        if not(len(command['method']) > 0 and \
               len(command['resource']) > 0 and \
               len(command['protocol']) > 0):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Validation of method failed')
            self.debug('Validation of method failed')
            return None

        #must be a packet with 200 OK status code
        if not (command['method'] == 'http/1.1' and \
                command['resource'] == '200' and \
                command['protocol'] == 'ok'):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'Validation of method failed')
            self.debug('Validation of method failed')
            return None

        #following headers are mandatory for both packet types (alive and byebye)
        if not(headers.has_key('cache-control') \
               and headers.has_key('ext') \
               and headers.has_key('location') \
               and headers.has_key('server') \
               and headers.has_key('st') \
               and headers.has_key('usn')):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'mandatory header missing')
            self.debug('mandatory header missing')
            return None

        #cache-control must have max-age directive, should be >= 1800 seconds
        cache_control = headers['cache-control'].split('=')
        if not (len(cache_control) == 2 and \
                cache_control[0].strip() == 'max-age' and \
                cache_control[1].strip().isdigit()):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'max-age missing: %s' % headers['cache-control'])
            self.debug('max-age missing: %s' % headers['cache-control'])
            return None

        #retrieve UUID to ease tracking of faulty device
        usn_split = headers['usn'].split('::')
        uuid = usn_split[0][5:]

        #give hint about to low value for max-age
        if int(cache_control[1].strip()) < 1800:
            self._ssdp_max_age_to_low = True
            louie.send('UPnT.infoMessage', None, 'You should check the max-age (%s) for your device %s/%s. 1800 seconds is recommended.' % (cache_control[1].strip(), self.ip, uuid))

        #DATE header is recommended
        if not headers.has_key('date'):
            self._date_header_not_sent = True
            louie.send('UPnT.infoMessage', None, 'You should include a DATE header in your M-SEARCH responses (%s/%s).' % (self.ip, uuid))


        #value for EXT header has to be empty
        if len(headers['ext']) > 0:
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'EXT header has a value')
            self.debug('EXT header has a value')
            return None

        #LOCATION must be a valid URL
        if not len(headers['location']):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'LOCATION empty')
            self.debug('LOCATION empty')
            return None

        #reachability of location checked at end of function

        #SERVER has to have UPnP/1.0 in the string
        if headers['server'].find('UPnP/1.0') == -1:
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'SERVER doesn\'t contain UPnP/1.0')
            self.debug('SERVER doesn\'t contain UPnP/1.0')
            return None

        #ST is similar to NT
        st_headers = headers['st'].split(':')
        st_headers = filter(lambda x: len(x) > 0, st_headers)
        if not (len(st_headers) == 2 \
                or len(st_headers) == 5):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'error in ST header')
            self.debug('error in ST header')
            return None

        if len(st_headers) == 2 and \
            (not (headers['st'] == 'upnp:rootdevice' or \
                  headers['st'].startswith('uuid:'))):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'error in ST header')
            self.debug('error in ST header')
            return None
        #further testing possible?

        #USN header
        if not headers['usn'].startswith('uuid:'):
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'error in USN header')
            self.debug('error in USN header')
            return None

        usn_headers = headers['usn'].split('::')
        if len(usn_headers) == 2:
            if not (usn_headers[1] == 'upnp:rootdevice' or \
                    (usn_headers[1].startswith('urn:') and \
                     len(usn_headers[1].split(':')) == 5)):
                louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'error in USN header')
                self.debug('error in USN header')
                return None

        #check if 2nd part of USN header is equal to NT header if NT header is not the UUID
        if headers['st'].startswith('uuid:'):
            if headers['st'] != headers['usn']:
                return None
        else:
            usn_split = headers['usn'].split('::')
            if headers['st'] != usn_split[1]:
                return None

        #SSDP only needs the header data
        if len(payload) > 0:
            louie.send('UPnT.errorMessage', None, 'HostValidationError (%s)' % self.ip, 'packet has payload')
            self.debug('packet has payload')
            return None

        self.debug('Trying to fetch description...')
        d = None
        if self._config.get('use_HEAD_request', False):
            d = utils.getPage(headers['location'], method='HEAD')
        else:
            d = utils.getPage(headers['location'])
        #louie.send('UPnT.running_deferred', None, d)
        return d

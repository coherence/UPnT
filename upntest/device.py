"""
Module that holds the device classes for client and server devices. Even though
all UPnP devices have a server and a client part the distinction is made between
UPnP devices that offer resources to other UPnP devices and the ones that just
use/control them, ususally a UPnP ControlPoint.

@license: Licensed under the MIT license
    http://opensource.org/licenses/mit-license.php
@author: Michael Weinrich <testsuite@michael-weinrich.de>
@copyright: Copyright 2007, Michael Weinrich
"""

# external includes
from lxml import etree
from StringIO import StringIO
from twisted.internet import reactor, defer
from twisted.python.filepath import FilePath
import louie
import urllib2
# Coherence includes
from coherence import log
from coherence.upnp.core import utils
# local includes
from upntest.controlling import ClientControl
from upntest.eventing import ClientEventMessageChecks
from upntest.service import MissingUrlException


class ClientDevice(log.Loggable):
    """
    This class keeps track of messages sent by a client device (most likely an
    UPnP ControlPoint).
    """

    logCategory = 'UPnT_ClientDevice'
    """Log category for client devices."""

    eventchecker = None
    """Class variable holding the L{ClientEventMessageChecks} instance. This is
    needed only once hence implemented as a static class variable. It checks all
    incoming event messages for validity."""
    controlchecker = None
    """Class variable holding the L{ClientControl} instance. This is needed only
    once hence implemented as a static class variable. It checks all incoming
    control messages for validity."""

    def __init__(self, config):
        """
        Intialise the client device checker.

        Checks for already instantiated L{eventchecker} and L{controlchecker}.
        If one of them is still C{None}, a new instance is created.

        @param config: A C{dict} containing the configuration data from the
        config file.
        """

        templates_dir = config.get('templates_dir', FilePath(__file__).parent().parent().child('xmlfiles').path)

        if not self.eventchecker:
            self.eventchecker = ClientEventMessageChecks()
        if not self.controlchecker:
            self.controlchecker = ClientControl(templates_dir)


class ServerDevice(log.Loggable):
    """
    This class stores and checks data about server devices like MediaServers or
    MediaRenderers.
    """

    logCategory = 'UPnT_ServerDevice'
    """Log category for server devices."""

    def __init__(self, headers, config):
        """
        Initialise the L{ServerDevice}.

        @param headers: A C{dict} conataining the headers of the received SSDP
        packet.
        @param config: A C{dict} containing the configuration data from the
        config file.
        """

        self._uuid = ''
        """UUID of this device."""
        self._parent = None
        """A L{ServerDevice} instance that is the parent device to this one or
        None if this device is the root device."""
        self._host = None
        """The L{Host} where this device resides. Needed to ease tracking down
        errors that are displayed."""
        self._rootDevice = False
        """A C{bool} indicating whether this device is a root device or not."""
        self._schema = ''
        """The schema of this device (C{str})."""
        self._deviceType = ''
        """The type of this device (C{str})."""
        self._deviceVersion = ''
        """The version of this device type (C{str})."""
        self._descLocation = ''
        """The URL where the desciption can be downloaded."""
        self._urlBase = ''
        """The base URL with which every realtiv URL in the description is
        preceded with."""
        self._subDevices = {}
        """A C{dict} containing all devices that are subdevice to this device."""
        self._services = {}
        """A C{dict} containing all services this device offers."""

        self._xmlschema = None
        """An XML schema that is used to validate the description of this
        device."""
        schema_file_dir = config.get('schema_file_dir', FilePath(__file__).parent().parent().child('schemas').path)

        if not schema_file_dir.endswith('/'):
            schema_file_dir += '/'

        if schema_file_dir is not None:
            xmlschema_doc = etree.parse(schema_file_dir + 'device.xsd')
            self._xmlschema = etree.XMLSchema(xmlschema_doc)

        self._expirationNotificationFunc = None
        """An object which provides C{IDelayedCall} for L{deviceExpired} which
        shows an error message when the maximum age of the device announcement
        is reached."""

        # parse data
        self.update(headers, True)

    def __str__(self):
        """
        Redefines the string representation to show the full device name
        consisting of the UUID, the device type and the version of this device.
        """
        return self._uuid + '::' + self._deviceType + ':' + self._deviceVersion

    def getLocation(self):
        """
        Get the URL of the description of this device.

        @return: The URL as C{str}.
        """
        return self._descLocation
    location = property(getLocation)
    """The URL of the description of this device."""

    def getDeviceType(self):
        """
        Get the device type of this device.

        @return: The device type as C{str}.
        """
        return self._deviceType
    deviceType = property(getDeviceType)
    """The device type of this device."""

    def getDeviceVersion(self):
        """
        Get the version of the device type.

        @return: The version as C{str}.
        """
        return self._deviceVersion
    deviceVersion = property(getDeviceVersion)
    """The version of the device type."""

    def getUuid(self):
        """
        Get the UUID of this device.

        @return: The UUID as C{str}.
        """
        return self._uuid
    UUID = property(getUuid)
    """The UUID of this device."""

    def getParent(self):
        """
        Get the parent device of this device.

        @return: The parent as L{ServerDevice}.
        """
        return self._parent

    def setParent(self, parent_device):
        """
        Set the parent device of this device.

        @param parent_device: The L{ServerDevice} that is parent device of this
        one.
        """
        self._parent = parent_device
    parent = property(getParent, setParent)
    """The parent L{ServerDevice} of this device."""

    def getHost(self):
        """
        Get the L{Host} where this device resides.

        @return: A L{Host} object.
        """
        return self._host

    def setHost(self, host):
        """
        Set the L{Host} where this device resides.

        @param host: The L{Host}.
        """
        self._host = host
    host = property(getHost, setHost)
    """The L{Host} where this device resides."""

    def getSubDevices(self):
        """
        Get all devices that are part of this device.

        @return: A L{dict} containing all subdevices.
        """
        return self._subDevices
    subDevices = property(getSubDevices)
    """All subdevices of this device (C{dict})."""

    def getServices(self):
        """
        Get the L{Host} where this device resides.

        @return: A L{Host} object.
        """
        return self._services
    services = property(getServices)
    """The L{Host} where this device resides."""

    def isRootDevice(self):
        """
        Get the the root device status of this device.

        @return: A L{bool} indicating whether this device is a root device or
        not.
        """
        return self._rootDevice

    def update(self, headers, new=False):
        """
        Update this device with information from the given headers. This is
        needed because a root device is announced several times. With one packet
        it is marked as root device and another packet it informs us about the
        actual device type of this device.

        @param headers: A C{dict} conataining the headers of the received SSDP
        packet.
        @param new: Indicator whether this device was just created or if it
        will be updated with the information for the SSDP packet.
        """

        usn_split = headers['usn'].split('::')
        # get seconds for device expiration and set timeout for warning
        cache_control = headers['cache-control'].split('=')

        if new:
            self.info('Creating new device for %s' % usn_split[0][5:])
            self._uuid = usn_split[0][5:]  # UUID is uuid:1234567890
            self._expirationNotificationFunc = \
                reactor.callLater(int(cache_control[1].strip()),
                                  self.deviceExpired)
        else:
            self.info('Updating device for %s' % usn_split[0][5:])
            self._expirationNotificationFunc.reset(int(cache_control[1].strip()))

        self._descLocation = headers['location']
        self.debug(self._descLocation)

        parsed = urllib2.urlparse.urlparse(self._descLocation)
        self.url_base = "%s://%s" % (parsed[0], parsed[1])

        if len(usn_split) > 1:
            usn_right_parts = usn_split[1].split(':')

            if len(usn_right_parts) > 2:
                self._schema = usn_right_parts[0] + ':' + usn_right_parts[1] + \
                    ':' + usn_right_parts[2]
                self._deviceType = usn_right_parts[3]
                self._deviceVersion = usn_right_parts[4]
            else:
                self._rootDevice = True

    def addSubdevice(self, subdevice):
        """
        Add a L{ServerDevice} as subdevice to this device.

        @param subdevice: A L{ServerDevice} object that should be subdevice to
        this one.
        """

        self.debug('Assigning %s:%s (%s) as parentDevice to %s/%s:%s' % \
                   (self._deviceType,
                    self._deviceVersion,
                    self._uuid,
                    self._host.ip,
                    subdevice.deviceType,
                    subdevice.deviceVersion))

        subdevice.parent = self
        self._subDevices[subdevice.UUID] = subdevice

    def addService(self, service, usn):
        """
        Add a L{Service} as service to this device.

        @param service: A L{Service} object that is adde as service to this
        device.
        @param usn: The USN of the service that is added (C{str}).
        """

        self.debug('Assigning %s:%s (%s) as parentDevice to %s/%s:%s' %
                   (self._deviceType,
                    self._deviceVersion,
                    self._uuid,
                    self._host.ip,
                    service.serviceType,
                    service.serviceVersion))

        self._services[usn] = service
        service.parentDevice = self

    def checkDescriptions(self, device_list, service_list):
        """
        Check if description of device is valid and if all devices and
        services were announced hence existent in the L{_subDevices} and
        L{_services}.

        @param device_list: A C{dict} containing all devices that reside on the
        host.
        @param service_list: A C{dict} containing all services that are
        implemented on the host.

        @return: A C{Deferred} for executing aditional callbacks after the
        internal procesing.
        """

        self.debug('Checking device description from %s' % self._descLocation)
        d = utils.getPage(self._descLocation)
        #louie.send('UPnT.running_deferred', None, d)

        d.addCallback(self.validateDescription)
        d.addErrback(self.validateDescriptionFailed,
                     self._descLocation)

        d.addCallback(self.checkDeviceAnnouncement,
                      device_list)
        d.addErrback(self.checkDeviceAnnouncementFailed,
                     self._descLocation)

        d.addCallback(self.checkServiceAnnouncement,
                      device_list,
                      service_list,
                      d=d)
        d.addErrback(self.checkServiceAnnouncementFailed,
                     self._descLocation,
                     d=d)

        return d

    def validateDescription(self, desc):
        """
        Description was sucessfully received and is validated against the schema
        of the UPnP Device Architecture (UDA) that is stored in L{_xmlschema}.

        @param desc: The description that was retrieved from the device (C{str}).

        @return: The description that was retrieved from the device for further
        processing (C{lxml.etree}).
        """

        self.debug('validateDescription')
        #self.debug(desc)
        description, headers = desc

        doc = etree.parse(StringIO(description))
        description = self.removeUnknownTags(doc)

        if self._xmlschema is None:
            louie.send('UPnT.errorMessage', None, 'DeviceDescriptionError', 'No XML Schema to compare description against!')
            return None
        elif self._xmlschema.validate(doc) == 1:
            #louie.send('UPnT.infoMessage', None, 'Device-Description valid! (UUID %s)' % self._uuid)
            pass
        else:
            error = self._xmlschema.error_log.last_error
            self.warning(error)
            louie.send('UPnT.errorMessage', None, 'DeviceDescriptionError', 'Device-Description not valid! (UUID %s)' % self._uuid)
            self.info('Description:\n%s' % description)
            raise InvalidDescriptionException(self._uuid)

        return doc

    def validateDescriptionFailed(self, failure, url):
        """
        Error while receiving description.

        @param failure: Message about the failure that happened.
        @param url: URL where the description should be located.

        @return: The failure so that the error chain continues and the other
        errbacks are called.
        """

        self.warning("validateDescriptionFailed %r" % url)
        self.debug(failure)
        return failure

    def checkDeviceAnnouncement(self, doc, device_list):
        """
        Take the description, extract devices and look for a ServerDevice object
        in the existing devices list. If found, add it to the tree. If not, the
        device wasn't announced.

        @param doc: The description of the device as C{lxml.etree}.
        @param device_list: A C{dict} containing all devices that reside on the
        host.

        @return: The description that was retrieved from the device for further
        processing (C{lxml.etree}).
        """

        self.debug('checkDeviceAnnouncement')

        # some error happened in previous callbacks
        if doc == None:
            return None

        def checkRecursive(device_desc, parent, device_list, xpath_exp='/upnp:root/upnp:device'):
            """
            Parse the current list of devices and check if the device was
            announced during discovery phase. If it was announced assign the
            device given in C{parent} as parent device for this one. Afterwards
            look for subdevices that are listed in the description. If there are
            any, do the same to them until no subdevices are found.

            @param device_desc: A C{lxml.etree} object representing the device
            list for the current parent device.
            @param parent: A L{ServerDevice} that is the parent device for all
            devices found at the current recursion level.
            @param device_list: A C{dict} containing all devices that reside on
            the host.
            @param xpath_exp: An XPath expression giving the path to the current
            list of devices.
            """

            ns = {'upnp': 'urn:schemas-upnp-org:device-1-0'}
            devices = device_desc.xpath(xpath_exp, ns)

            for device in devices:
                udn = device.xpath('upnp:UDN', ns)
                uuid = udn[0].text[5:]
                deviceType = device.xpath('upnp:deviceType', ns)
                if not device_list.has_key(uuid):
                    louie.send('UPnT.errorMessage', None, 'DeviceError', 'Device %s/uuid:%s::%s wasn\'t announced during discovery phase' % (self.host.ip, uuid, deviceType[0].text))
                else:
                    if self._uuid != uuid:
                        parent.addSubdevice(device_list[uuid])
                    #louie.send('UPnT.infoMessage', None, 'Device %s/uuid:%s::%s was announced during discovery phase' % (self.host.ip, uuid, deviceType[0].text))
                    checkRecursive(device, device_list[uuid], device_list, 'upnp:deviceList/upnp:device')

        checkRecursive(doc, self, device_list)
        return doc

    def checkDeviceAnnouncementFailed(self, failure, url):
        """
        Error while checking device announcements.

        @param failure: Message about the failure that happened.
        @param url: URL where the description should be located.

        @return: The failure so that the error chain continues and the other
        errbacks are called.
        """

        print 'checkDeviceAnnouncementFailed (%s)' % url
        self.debug('checkDeviceAnnouncementFailed (%s)' % url)
        self.debug(failure)
        return failure

    def checkServiceAnnouncement(self, doc, device_list, service_list, xpath_exp='/upnp:root/upnp:device', d=None):
        """
        Take the description, extract services and look for a L{Service}
        object in the existing services list. If found, add it to the tree. If
        not, the service wasn't announced.

        @param doc: The description of the device as C{lxml.etree}.
        @param device_list: A C{dict} containing all devices that reside on the
        host.
        @param service_list: A C{dict} containing all services that are
        implemented on the host.
        @param xpath_exp: An XPath expression giving the path to the current
        list of devices. Used to call this method recursively for subservices.
        @param d: C{Deferred} that was used to process the description. Used to
        tell the main L{UPnT} instance that the processing chain of this
        C{Deferred} was executed.


        @return: The description that was retrieved from the device for further
        processing (C{lxml.etree}).
        """
        self.debug('checkServiceAnnouncement')

        ns = {'upnp': 'urn:schemas-upnp-org:device-1-0'}
        devices = doc.xpath(xpath_exp, ns)

        for device in devices:
            udn = device.xpath('upnp:UDN', ns)
            uuid = udn[0].text[5:]
            if device_list.has_key(uuid):
                device_object = device_list[uuid]
                services = device.xpath('upnp:serviceList/upnp:service', ns)

                for service in services:
                    serviceType = service.xpath('upnp:serviceType', ns)
                    usn = udn[0].text + '::' + serviceType[0].text
                    if not service_list.has_key(usn):
                        louie.send('UPnT.infoMessage', None, 'Service %s/%s wasn\'t announced during discovery phase' % (self.host.ip, usn))
                    else:
                        #louie.send('UPnT.infoMessage', None, 'Service %s/%s was announced during discovery phase' % (self.host.ip, usn))
                        scpdurl = service.xpath('upnp:SCPDURL', ns)
                        if len(scpdurl) > 0:
                            service_list[usn].setScpdUrl(scpdurl[0].text)

                        controlurl = service.xpath('upnp:controlURL', ns)
                        if len(controlurl) > 0:
                            service_list[usn].setControlUrl(controlurl[0].text)

                        eventsuburl = service.xpath('upnp:eventSubURL', ns)
                        if len(eventsuburl) > 0:
                            service_list[usn].setEventSubUrl(eventsuburl[0].text)

                        device_object.addService(service_list[usn], usn)

                        try:
                            self.debug('checking service %s' % usn)
                            device_object._services[usn].checkDescriptions()
                        except MissingUrlException, error:
                            self.info(error)

                self.checkServiceAnnouncement(device, device_list, service_list, xpath_exp='upnp:deviceList/upnp:device')
        #if d is not None:
        #    louie.send('UPnT.ending_deferred', None, d)

        return doc

    def checkServiceAnnouncementFailed(self, failure, url, d):
        """
        Error while checking service announcements. This is the last of the
        errback functions for this processing chain. therefore it sends a signal
        to the main L{UPnT} instance telling that this C{Deferred} has finished
        processing.

        @param failure: Message about the failure that happened.
        @param url: URL where the description should be located.
        @param d: C{Deferred} that was used to process the description. Used to
        tell the main L{UPnT} instance that the processing chain of this
        C{Deferred} was executed.
        """

        self.debug('checkServiceAnnouncementFailed (%s)' % url)
        self.debug(failure)
        #louie.send('UPnT.ending_deferred', None, d)

    def removeUnknownTags(self, xmldoc):
        """
        Removes unknown elements from tree before validating.

        This method is mainly used to remove DLNA elements because I don't have
        the specs and therefore I have no XML schema to validate them.

        @param xmldoc: A C{lxml.etree} object holding the document to filter.

        @return: The filtered document (C{lxml.etree}).
        """

        MAIN_NS = '{urn:schemas-upnp-org:device-1-0}'
        """The main UPnP namespace used to filter tags of other namespaces
        (like DLNA)."""

        unknown_elements = [MAIN_NS + 'INMPR03']
        """Other non-standard elements known for being send by certain devices."""

        for el in xmldoc.getiterator('*'):
            if not el.tag.startswith(MAIN_NS) or el.tag in unknown_elements:
                parent = el.getparent()
                if parent is not None:  # not the root element
                    parent.remove(el)
        return xmldoc

    def deviceExpired(self):
        """
        Print a message that the announcement of this device has expired.
        """

        self._expirationNotificationFunc = None
        louie.send('UPnT.infoMessage', None, 'Device announcement of %s:%s has expired!' % (self._deviceType, self._deviceVersion))

    def removeService(self, headers):
        """
        Clean up and emove a service from this device.

        @param headers: The headers of the SSDP ByeBye packet (C{dict}).
        """
        if self._services.has_key(headers['usn']):
            self.debug('Sending UPnT.serviceByeBye...')
            louie.send('UPnT.serviceByeBye', None, self._services[headers['usn']])
            if self._services[headers['usn']].eventchecker is not None:
                self._services[headers['usn']].eventchecker.removeEventSeq()
            del self._services[headers['usn']]


class InvalidDescriptionException(StandardError):
    """
    Error representing an invalid device description.
    """

    def __init__(self, uuid):
        """
        Initialise the error with the UUID of the device where the exception
        was raised.

        @param uuid: The UUID of the device where the exception was raised.
        """
        self.uuid = uuid

    def __str__(self):
        """
        Create a string representation of the exception.

        @return: The string representation of this exception.
        """

        return 'Device-Description not valid! (UUID %s)' % (self.uuid)

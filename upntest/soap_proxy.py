"""
taken from Coherence source an modified by Michael Weinrich according to the
needs of UPnT

@license: Licensed under the MIT license 
    http://opensource.org/licenses/mit-license.php
@author: Frank Scholz <coherence@beebits.net>
@copyright: Copyright 2007 - Frank Scholz <coherence@beebits.net>
"""

from coherence.extern.et import ET, namespace_map_update
from lxml import etree
from StringIO import StringIO

from coherence.upnp.core.utils import getPage, parse_xml

from upntest import soap_lite
from coherence import log


class SOAPProxy(log.Loggable):
    logCategory = 'UPnT_SOAPProxy'
    """ A Proxy for making remote SOAP calls.

        Based upon twisted.web.soap.Proxy and
        extracted to remove the SOAPpy dependency

        Pass the URL of the remote SOAP server to the constructor.

        Use proxy.callRemote('foobar', 1, 2) to call remote method
        'foobar' with args 1 and 2, proxy.callRemote('foobar', x=1)
        will call foobar with named argument 'x'.
    """

    def __init__(self, url, namespace=None, envelope_attrib=None, header=None, soapaction=None):
        """
        Initialise the proxy.
        
        @param url: 
        @param namespace: 
        @param envelope_attrib: 
        @param header: 
        @param soapaction: 
        """
        self.url = url
        self.namespace = namespace
        self.header = header
        self.action = None
        self.soapaction = soapaction
        self.envelope_attrib = envelope_attrib

    def callRemote(self, soapmethod, in_args, *args, **kwargs):
        """
        Invoke action soapmethod on the remote SOAP server with in_args as 
        arguments.
        
        @param soapmethod: 
        @param in_args: 
        """

        soapaction = self.soapaction or soapmethod
        self.action = soapmethod

        ns = self.namespace
        payload = soap_lite.build_soap_call("{%s}%s" % (ns[1], soapmethod), in_args,
                                            encoding=None)
        #self.debug("soapaction: %r" % soapaction)
        #self.debug("callRemote: %r" % payload)
        #self.debug("url: %r" % self.url)

        def gotError(failure, url):
            self.debug("error requesting %s" % url)
            self.debug(failure)
            return failure

        return getPage(self.url, postdata=payload, method="POST",
                        headers={'content-type': 'text/xml ;charset="utf-8"',
                                 'SOAPACTION': '"%s"' % soapaction,
                                }
                      ).addCallbacks(self._cbGotResult, gotError, None, None, [self.url], None)

    def _cbGotResult(self, result):
        """
        Process the returned result data..
        
        @param result: 
        """
        #print "_cbGotResult 1", result
        page, headers = result
        #result = SOAPpy.parseSOAPRPC(page)
        #print "_cbGotResult 2", result

        def print_c(e):
            for c in e.getchildren():
                print c, c.tag
                print_c(c)

        tree = parse_xml(page)
        #print etree.tostring(etree.parse(StringIO(page)), pretty_print=True)
        #print tree, "find %s" % self.action

        #root = tree.getroot()
        #print_c(root)

        body = tree.find('{http://schemas.xmlsoap.org/soap/envelope/}Body')
        #print "body", body
        response = body.find('{%s}%sResponse' % (self.namespace[1], self.action))
        if response == None:
            """ fallback for improper SOAP action responses """
            response = body.find('%sResponse' % self.action)
        #print "response", response
        if response == None:
            """ probably an error message """
            return self.decodeFaultMessage(body)
        else:
            result = {}
            if response != None:
                for elem in response:
                    result[elem.tag] = self.decode_result(elem)
            #print "_cbGotResult 3", result

            return result

    def decode_result(self, element):
        """
        Decode the returned result element.
        
        @param element: 
        """
        type = element.get('{http://www.w3.org/1999/XMLSchema-instance}type')
        if type is not None:
            try:
                prefix, local = type.split(":")
                if prefix == 'xsd':
                    type = local
            except ValueError:
                pass

        if type == "integer" or type == "int":
            return int(element.text)
        if type == "float" or type == "double":
            return float(element.text)
        if type == "boolean":
            return element.text == "true"

        return element.text or ""

    def decodeFaultMessage(self, body):
        """
        Decode the returned fault message in a human readable format.
        
        @param body: 
        """
        response = body.find('{http://schemas.xmlsoap.org/soap/envelope/}Fault')
        #print 'response: %r' % response
        result = {}
        if response != None:
            for elem in response:
                #print 'elem: %r' % elem.tag
                if elem.tag == 'detail':
                    error_elems = elem.find('.//{urn:schemas-upnp-org:control-1-0}UPnPError')
                    #print 'error_elems: %r' % error_elems
                    if error_elems:
                        for e_elem in error_elems:
                            #print 'e_elem: %r' % e_elem.tag
                            result[e_elem.tag] = e_elem.text
                else:
                    result[elem.tag] = elem.text

        return result

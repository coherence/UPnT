"""
Module that contains the Service class and several Exception classes that may be
raised during processing.

@license: Licensed under the MIT license
    http://opensource.org/licenses/mit-license.php
@author: Michael Weinrich <testsuite@michael-weinrich.de>
@copyright: Copyright 2007, Michael Weinrich
"""

# external includes
import louie
from exceptions import StandardError
from lxml import etree
from os import path
from StringIO import StringIO
from twisted.internet import reactor, defer
from twisted.python.filepath import FilePath
from twisted.python import failure
from urllib2 import urlparse
# Coherence includes
from coherence import log
from coherence.upnp.core import utils
# local includes
from upntest.controlling import ServerControl
from upntest.eventing import ServerEventMessageChecks


class Service(log.Loggable):
    """
    This class checks information of a service that is retrieved from the
    device offering the service and stores that information for later usage.
    """

    logCategory = 'UPnT_Service'
    """Log category for a service."""

    def __init__(self, headers, config):
        """
        Initialise the service.

        @param headers: A C{dict} conataining the headers of the received SSDP
        packet.
        @param config: A C{dict} containing the configuration data from the
        config file.
        """

        self._config = config
        """The configuration data from the config file C{dict})."""
        self._parentDevice = None
        """A L{ServerDevice} instance that is the parent device to this service.
        """
        self._schema = ''
        """The schema of this service (C{str})."""
        self._serviceType = ''
        """The type of this service (C{str})."""
        self._serviceVersion = ''
        """The version of this service type (C{str})."""
        self._SCPDURL = ''
        """The URL where the desciption of the servcie can be downloaded."""
        self._controlURL = ''
        """The URL where the control messages have to be sent to."""
        self._eventSubURL = ''
        """The URL where the subscribe and unsubscribe messages have to be sent
        to."""
        self._urlBase = ''
        """The base URL with which every realtiv URL in the description is
        preceded with."""
        self._expirationNotificationFunc = None
        """An object which provides C{IDelayedCall} for L{serviceExpired} which
        shows an error message when the maximum age of the device announcement
        is reached."""
        self._actionList = {}
        """A C{dict} containing all actions this device offers."""

        self._xmlschema = None
        """An XML schema that is used to validate the description of this
        service."""
        schema_file_dir = config.get('schema_file_dir', FilePath(__file__).parent().parent().child('schemas').path)

        if not schema_file_dir.endswith('/'):
            schema_file_dir += '/'

        if schema_file_dir is not None:
            xmlschema_doc = etree.parse(schema_file_dir + 'service.xsd')
            self._xmlschema = etree.XMLSchema(xmlschema_doc)

        self.templates_dir = config.get('templates_dir', FilePath(__file__).parent().parent().child('xmlfiles').path)
        """Path to the directory where the service and device templates are
        located (C{str})."""

        if not self.templates_dir.endswith('/'):
            self.templates_dir += '/'

        self.eventchecker = None
        """A L{ServerEventMessageChecks} object responsible for checking event
        messages sent by the event server of this service."""
        self.event_connection = None
        """Variable used by the C{Coherence} event subscription routine."""
        self._subscriptionId = ''
        """Unique Subscription ID received with a successful subscription."""
        self.serverControl = None
        """A L{ServerControl} object responsible for sending control messages
        and check the responses."""
        self.update(headers, True)

    def update(self, headers, new=False):
        """
        Update this service with information from the ssdp:alive paket.

        @param headers: A C{dict} conataining the headers of the received SSDP
        packet.
        @param new: If C{False} this service was just created. If C{True} this
        service is updated with the information of the SSDP packet.
        """

        usn_split = headers['usn'].split('::')
        usn_right_parts = usn_split[1].split(':')

        self._schema = usn_right_parts[0] + ':' + usn_right_parts[1] + ':' + usn_right_parts[2]
        self._serviceType = usn_right_parts[3]
        self._serviceVersion = usn_right_parts[4]

        parsed = urlparse.urlparse(headers['location'])
        self._urlBase = "%s://%s" % (parsed[0], parsed[1])

        # get seconds for device expiration and set timeout for warning
        cache_control = headers['cache-control'].split('=')

        if new:
            self.info('New service of type %s created on %s' % (self._serviceType, usn_split[0][5:]))
            self._expirationNotificationFunc = reactor.callLater(int(cache_control[1].strip()), self.serviceExpired)
        else:
            self.info('Updating service of type %s on %s' % (self._serviceType, usn_split[0][5:]))
            self._expirationNotificationFunc.reset(int(cache_control[1].strip()))

    def __str__(self):
        """
        Get a string representation of this service.

        @return: A string representation of this service.
        """
        return self.parentDevice.UUID + '::' + self._serviceType + ':' + self._serviceVersion

    def getType(self):
        """
        Get the type of this service.

        @return: The type of this service (C{str}).
        """
        return self._serviceType
    serviceType = property(getType)
    """The type of the service (C{str})."""

    def getVersion(self):
        """
        Get the version of this service.

        @return: The version of this service (C{str}).
        """
        return self._serviceVersion
    serviceVersion = property(getVersion)
    """The version of the service (C{str})."""

    def getSchema(self):
        """
        Get the schema of this service.

        @return: The schema of this service (C{str}).
        """
        return self._schema
    schema = property(getSchema)
    """The schema of the service (C{str})."""

    def getParentDevice(self):
        """
        Get the parent device of this service.

        @return: The type of this service (C{str}).
        """
        return self._parentDevice

    def setParentDevice(self, parentDevice):
        """
        Set the parent device of this service.

        @param parentDevice: New parent device of this service (L{ServerDevice}).
        """
        if self.serverControl is not None:
            self.serverControl.stopTesting()
            self.serverControl = None
        if self.eventchecker is not None:
            self.eventchecker.cancelSubscription()
            self.eventchecker = None
        self._parentDevice = parentDevice
        self.serverControl = ServerControl(self, self._config, self._serviceType, self._serviceVersion, self.parentDevice.UUID)
        self.eventchecker = ServerEventMessageChecks(self, self._config)
    parentDevice = property(getParentDevice, setParentDevice)
    """The parent device of this service (L{ServerDevice})."""

    def getEventSubUrl(self):
        """
        Get the URL where to send messages to subscribe and unsubscribe.

        @return: A URL (C{str}).
        """
        return self._eventSubURL

    def setEventSubUrl(self, eventSubURL):
        """
        Set the URL where to send messages to subscribe and unsubscribe.

        @param eventSubURL: New URL for service subscription messages.
        """
        if not eventSubURL.startswith('/'):
            self._eventSubURL = '/' + eventSubURL
        else:
            self._eventSubURL = eventSubURL
    eventSubUrl = property(getEventSubUrl, setEventSubUrl)
    """The URL where the subscribe and unsubscribe messages have to be sent to.
    """

    def getScpdUrl(self):
        """
        Get the URL where the description of this service can be retrieved.

        @return: A URL (C{str}).
        """
        return self._SCPDURL

    def setScpdUrl(self, scpdUrl):
        """
        Set the URL where to send messages to subscribe and unsubscribe.

        @param scpdUrl: New URL for the service description.
        """
        if not scpdUrl.startswith('/'):
            self._SCPDURL = '/' + scpdUrl
        else:
            self._SCPDURL = scpdUrl
    scpdUrl = property(getScpdUrl, setScpdUrl)
    """The URL where the desciption of the servcie can be downloaded."""

    def getControlUrl(self):
        """
        Get the URL where the control messages have to be send to.

        @return: A URL (C{str}).
        """
        return self._controlURL

    def setControlUrl(self, controlUrl):
        """
        Set the URL where the control messages have to be send to.

        @param controlUrl: New URL for control messages.
        """
        if not controlUrl.startswith('/'):
            self._controlURL = '/' + controlUrl
        else:
            self._controlURL = controlUrl
    controlUrl = property(getControlUrl, setControlUrl)
    """The URL where the control messages have to be sent to."""

    # interface compatibility functions
    def get_base_url(self):
        """
        Get the base URL with which every realtiv URL in the description is
        preceded with (compatibility funciton, used by
        C{coherence.upnp.core.event.subscribe}).

        @return: A URL (C{str}).
        """
        return self._urlBase

    def get_event_sub_url(self):
        """
        Get the URL where the subscribe and unsubscribe messages have to be sent
        to.

        @return: A URL (C{str}).
        """
        return self._eventSubURL

    def get_sid(self):
        """
        Get the Subscription ID received after the successful subscription.

        @return: A subscription ID (C{int}).
        """
        return self._subscriptionId

    def checkDescriptions(self):
        """
        Check the device data for correctness.

        First the SCDPURL and the controlURL are checked for emptiness.
        Afterwards the description is retrieved and several functions are
        executed to check the validity of the description.
        """

        uuid = self.parentDevice.UUID
        if len(self._SCPDURL) == 0:
            raise MissingUrlException('Service %s/%s has no SCPDURL' % (self.parentDevice.host.ip, uuid))
        if len(self._controlURL) == 0:
            raise MissingUrlException('Service %s/%s has no controlURL' % (self.parentDevice.host.ip, uuid))

        self.debug('Checking service description from %s' % self._urlBase + self._SCPDURL)
        d = utils.getPage(self._urlBase + self._SCPDURL)
        #louie.send('UPnT.running_deferred', None, d)

        d.addCallback(self.validateDescription)
        d.addErrback(self.validateDescriptionFailed, self._urlBase + self._SCPDURL)

        d.addCallback(self.validateDescriptionContent)
        d.addErrback(self.validateDescriptionContentFailed, self._urlBase + self._SCPDURL)

        if not self._config.get('GUI', False):
            self.debug('Starting Control Testing')
            d.addCallback(self.startControlTesting, d)
            d.addErrback(self.startControlTestingFailed, self._urlBase + self._SCPDURL, d)

    def validateDescription(self, desc):
        """
        Description was sucessfully received and will be checked if its XML is
        well-formed.

        @param desc: The description retrieved from the service (C{str}).

        @return: The description as C{lxml.etree} for further processing in the
        callback chain.
        """

        self.info('validateDescription')
        description, headers = desc

        doc = etree.parse(StringIO(description))

        if self._xmlschema is None:
            louie.send('UPnT.errorMessage', None, 'ServiceDescriptionError', 'No XML Schema to compare description against!')
            return defer.fail()
        elif self._xmlschema.validate(doc) == 1:
            #louie.send('UPnT.infoMessage', None, 'Service-Description valid! (%s::%s)' % (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion))
            pass
        else:
            error = self._xmlschema.error_log.last_error
            self.warning(error)
            louie.send('UPnT.infoMessage', None, 'Service-Description not valid! (%s::%s)' % (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion))
            self.debug('Service-Description:\n%s' % description)
            return defer.fail()

        return doc

    def validateDescriptionFailed(self, failure, url):
        """
        An error occured while receiving or checking the description.

        @param failure: Message about the failure that happened.
        @param url: URL where the description should be located.

        @return: The failure so that the error chain continues and the other
        errbacks are called.
        """

        self.info('validateDescriptionFailed')
        self.warning("error requesting", url)
        self.info(failure)
        return failure

    def checkRequiredActions(self, template_action_list, description_action_list, ns):
        """
        Check, if all required actions are present.

        @param template_action_list: A C{list} containing all actions from the
        service template.
        @param description_action_list: A C{list} containing all actions from
        the description.
        @param ns: The namespace of the elements in the description.

        @raise MissingRequiredActionException: If an action which is required
        is missing in the description hence not implemented by the service.
        """

        self.info('checkRequiredActions')

        action_required = {}
        for action in template_action_list:
            name = action.xpath('name')
            action_required[name[0].text.strip()] = (len(action.xpath('Optional')) == 0)

        description_action_names = []
        for action in description_action_list:
            description_action_names.append(action.xpath('upnp:name', ns)[0].text.strip())

        for action_name, required in action_required.iteritems():
            if required and not (action_name in description_action_names):
                raise MissingRequiredActionException(action_name, self.parentDevice.UUID + '::' + self._serviceType + ':' + self._serviceVersion)

    def checkRequiredStateVars(self, template_state_var_list, description_state_var_list, ns):
        """
        Check, if all required state variables are present.

        @param template_state_var_list: A C{list} containing all state variables
        from the service template.
        @param description_state_var_list: A C{list} containing all state
        variables from the description.
        @param ns: The namespace of the elements in the description.

        @raise MissingRequiredStateVariableException: If an state variable which
        is required is missing in the description hence not implemented by the
        service.
        """

        self.info('checkRequiredStateVars')

        state_var_required = {}
        for statevar in template_state_var_list:
            name = statevar.xpath('name')
            state_var_required[name[0].text.strip()] = (len(statevar.xpath('Optional')) == 0)

        description_state_var_names = []
        for statevar in description_state_var_list:
            description_state_var_names.append(statevar.xpath('upnp:name', ns)[0].text.strip())

        for statevar_name, required in state_var_required.iteritems():
            if required and not (statevar_name in description_state_var_names):
                raise MissingRequiredStateVariableException(statevar_name, self.parentDevice.UUID + '::' + self._serviceType + ':' + self._serviceVersion)

    def checkActionSyntax(self, template_action_list, description_action_list, ns):
        """
        Check, if all implemented actions are syntactically correct. While being
        at it, store all actions in a dictionary to be able to call all the
        actions for verifying control behaviour.

        @param template_action_list: A C{list} containing all actions from the
        service template.
        @param description_action_list: A C{list} containing all actions from
        the description.
        @param ns: The namespace of the elements in the description.

        @raise SyntaxError:
        If either of the following is true:
          - the direction of the argument doesn't match
          - the state variable that this action affects doesn't match
          - an argument is not found in the template
          - a method is not found in the template
        """

        self.info('checkActionSyntax')

        tpl_actions = {}
        for action_node in template_action_list:
            tpl_name = action_node.xpath('name')[0].text.strip()
            tpl_arguments = {}
            for argument in action_node.xpath('argumentList/argument'):
                tpl_arg_name = argument.xpath('name')[0].text.strip()
                #self.debug('ActionName %s, ArgumentName %s' % (tpl_name, tpl_arg_name))
                tpl_arg_dir = argument.xpath('direction')[0].text.strip()
                tpl_arg_state_var = argument.xpath('relatedStateVariable')[0].text.strip()
                tpl_arguments[tpl_arg_name] = {'direction': tpl_arg_dir, 'rel_state_var': tpl_arg_state_var}
            tpl_actions[tpl_name] = {}
            tpl_actions[tpl_name]['Arguments'] = tpl_arguments
            tpl_actions[tpl_name]['Optional'] = (len(action_node.xpath('Optional')) == 0)

        self._actionList = tpl_actions

        for action_node in description_action_list:
            action_name = action_node.xpath('upnp:name', ns)[0].text.strip()
            if tpl_actions.has_key(action_name):
                for argument in action_node.xpath('upnp:argumentList/upnp:argument', ns):
                    argument_name = argument.xpath('upnp:name', ns)[0].text.strip()
                    #self.debug('Checking argument %s for of action %s' % (argument_name, action_name))
                    if tpl_actions[action_name]['Arguments'].has_key(argument_name):

                        arg_dir = argument.xpath('upnp:direction', ns)[0].text.strip()
                        if tpl_actions[action_name]['Arguments'][argument_name]['direction'] != arg_dir:
                            raise SyntaxError('Wrong direction of argument %s (expected \'%s\', got \'%s\') ' % (argument_name,
                                                    tpl_actions[action_name]['Arguments'][argument_name]['direction'], arg_dir),
                                                    action_name,
                                                    self.parentDevice.UUID + '::' + self._serviceType + ':' + self._serviceVersion)

                        arg_state_var = argument.xpath('upnp:relatedStateVariable', ns)[0].text.strip()
                        if tpl_actions[action_name]['Arguments'][argument_name]['rel_state_var'] != arg_state_var:
                            raise SyntaxError('Wrong related state variable for argument %s (expected \'%s\', got \'%s\')' % (argument_name,
                                                    tpl_actions[action_name]['Arguments'][argument_name]['rel_state_var'], arg_state_var),
                                                    action_name,
                                                    self.parentDevice.UUID + '::' + self._serviceType + ':' + self._serviceVersion)
                    else:
                        self.debug('Argument %s for method %s not found in service template' % (argument_name, action_name))
                        raise SyntaxError('Argument %s for method not found in service template' % argument_name,
                                                action_name,
                                                self.parentDevice.UUID + '::' + self._serviceType + ':' + self._serviceVersion)
            else:
                self.debug('Method %s not found in service template' % action_name)
                raise SyntaxError('Method not found in service template',
                                        action_name,
                                        self.parentDevice.UUID + '::' + self._serviceType + ':' + self._serviceVersion)

            self._actionList[action_name] = tpl_actions[action_name]

    def checkStateVariableSyntax(self, template_state_var_list, description_state_var_list, ns):
        """
        Check, if all published state variables are syntactically correct.

        @param template_state_var_list: A C{list} containing all state variables
        from the service template.
        @param description_state_var_list: A C{list} containing all state
        variables from the description.
        @param ns: The namespace of the elements in the description.

        @raise SyntaxError: If a state variable is not found in the descrition.

        @return: C{True} if every state variable is ok, C{False} otherwise.
        """

        self.info('checkStateVariableSyntax')

        tpl_state_vars = {}
        for state_var_node in template_state_var_list:
            tpl_name = state_var_node.xpath('name')[0].text.strip()
            tpl_state_vars[tpl_name] = state_var_node

        result = True
        for state_var_node in description_state_var_list:
            state_var_name = state_var_node.xpath('upnp:name', ns)[0].text.strip()
            if tpl_state_vars.has_key(state_var_name):
                result = result and self.compareStateVariables(tpl_state_vars[state_var_name], state_var_node, ns)
            else:
                raise SyntaxError('State variable not found in service template',
                                        state_var_name,
                                        self.parentDevice.UUID + '::' + self._serviceType + ':' + self._serviceVersion)

        return result

    def compareStateVariables(self, elemTmpl, elemDesc, ns):
        """
        Compares two state variables for equality.

        @param elemTmpl: The C{lxml.etree} element containing the node of the
        state variable from the template.
        @param elemDesc: The C{lxml.etree} element containing the node of the
        state variable retrieved from the description.
        @param ns: The namespace of the elements in the description.

        @return: C{True} if the variables from the template and the description
        are equal, C{False} otherwise.
        """

        # extract information from template
        tpl_state_var = {}
        tpl_state_var['name'] = elemTmpl.xpath('name')[0].text.strip()
        tpl_state_var['dataType'] = elemTmpl.xpath('dataType')[0].text.strip()

        self.info('compareStateVariables for %s' % tpl_state_var['name'])

        tpl_def_val = elemTmpl.xpath('defaultValue')
        if len(tpl_def_val) > 0:
            tpl_state_var['defaultValue'] = tpl_def_val[0].text.strip()
        else:
            tpl_state_var['defaultValue'] = None

        tpl_allowed_list = elemTmpl.xpath('allowedValueList/allowedValue')
        tpl_allowed_range = elemTmpl.xpath('allowedValueRange')

        if len(tpl_allowed_list) > 0 and len(tpl_allowed_range) > 0:
            louie.send('UPnT.infoMessage', None, 'allowedValueList cannot be set in conjunction with allowedValueRange (%s::%s)\n%s\n%s' % \
              (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion, etree.tostring(elemTmpl, pretty_print=True),
               etree.tostring(elemDesc, pretty_print=True)))
            return False

        tpl_state_var['allowedValueList'] = []
        if len(tpl_allowed_list) > 0:
            tpl_state_var['allowedValueList'] = [allowedValue.text.strip() for allowedValue in tpl_allowed_list]

        tpl_state_var['allowedValueRange'] = {}

        if len(tpl_allowed_range) > 0:
            tpl_allowed_range_min = tpl_allowed_range[0].xpath('minimum')
            if len(tpl_allowed_range_min) > 0:
                tpl_state_var['allowedValueRange']['minimum'] = tpl_allowed_range_min[0].text.strip()

            tpl_allowed_range_max = tpl_allowed_range[0].xpath('maximum')
            if len(tpl_allowed_range_max) > 0:
                tpl_state_var['allowedValueRange']['maximum'] = tpl_allowed_range_max[0].text.strip()

            tpl_allowed_range_step = tpl_allowed_range[0].xpath('step')
            if len(tpl_allowed_range_step) > 0:
                tpl_state_var['allowedValueRange']['step'] = tpl_allowed_range_step[0].text.strip()

        tpl_send_events = elemTmpl.xpath('sendEventsAttribute')
        if len(tpl_send_events) > 0:
            tpl_state_var['sendEvents'] = tpl_send_events[0].text.strip()
        else:
            tpl_state_var['sendEvents'] = 'yes'

        # compare stored values against second element
        if tpl_state_var['name'] != elemDesc.xpath('upnp:name', ns)[0].text.strip():
            louie.send('UPnT.infoMessage', None, "Content of name tag doesn't match! (%s::%s)\n%s\n%s" % \
              (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion, etree.tostring(elemTmpl, pretty_print=True),
               etree.tostring(elemDesc, pretty_print=True)))
            return False
        if tpl_state_var['dataType'] != elemDesc.xpath('upnp:dataType', ns)[0].text.strip():
            louie.send('UPnT.infoMessage', None, "Content of dataType tag doesn't match! (%s::%s)\n%s\n%s" % \
              (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion, etree.tostring(elemTmpl, pretty_print=True),
               etree.tostring(elemDesc, pretty_print=True)))
            return False
        desc_def_val = elemDesc.xpath('upnp:defaultValue', ns)
        if len(desc_def_val) > 0 and tpl_state_var['defaultValue'] != None and \
          tpl_state_var['defaultValue'] != desc_def_val[0].text.strip():
            louie.send('UPnT.infoMessage', None, "Content of defaultValue tag doesn't match! (%s::%s)\n%s\n%s" % \
              (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion, etree.tostring(elemTmpl, pretty_print=True),
               etree.tostring(elemDesc, pretty_print=True)))
            return False

        desc_allowed_list = elemDesc.xpath('upnp:allowedValueList/upnp:allowedValue', ns)
        desc_allowed_range = elemDesc.xpath('upnp:allowedValueRange', ns)

        if len(desc_allowed_list) > 0 and len(desc_allowed_range) > 0:
            louie.send('UPnT.infoMessage', None, "allowedValueList cannot be set in conjunction with allowedValueRange (%s::%s)\n%s\n%s" % \
              (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion, etree.tostring(elemTmpl, pretty_print=True),
               etree.tostring(elemDesc, pretty_print=True)))
            return False

        if len(desc_allowed_list) > 0 and len(tpl_state_var['allowedValueList']) == 0:
            louie.send('UPnT.infoMessage', None, "allowedValueList is defined in service description, but not in template! (%s::%s)\n%s\n%s" % \
              (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion, etree.tostring(elemTmpl, pretty_print=True),
               etree.tostring(elemDesc, pretty_print=True)))
            return False

        if len(desc_allowed_list) == 0 and len(tpl_state_var['allowedValueList']) > 0:
            louie.send('UPnT.infoMessage', None, "allowedValueList is defined in template, but not in service description! (%s::%s)\n%s\n%s" % \
              (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion, etree.tostring(elemTmpl, pretty_print=True),
               etree.tostring(elemDesc, pretty_print=True)))
            return False

        if len(desc_allowed_list) > 0 and len(tpl_state_var['allowedValueList']) > 0:
            desc_allowedValueList = [allowedValue.text.strip() for allowedValue in desc_allowed_list]

            for allowedValue in tpl_state_var['allowedValueList']:
                if not allowedValue in desc_allowedValueList:
                    louie.send('UPnT.infoMessage', None, "Required value for allowedValueList not found in service description! (%s::%s)\n%s\n%s" % \
                      (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion, etree.tostring(elemTmpl, pretty_print=True),
                       etree.tostring(elemDesc, pretty_print=True)))
                    return False

        if len(desc_allowed_range) > 0:
            desc_allowed_range_min = desc_allowed_range[0].xpath('upnp:minimum', ns)
            if len(desc_allowed_range_min) > 0 and tpl_state_var['allowedValueRange'].has_key('minimum') and \
               tpl_state_var['allowedValueRange']['minimum'] != desc_allowed_range_min[0].text.strip():
                louie.send('UPnT.infoMessage', None, "Value for minimum doesn't match! (%s::%s)\n%s\n%s" % \
                  (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion, etree.tostring(elemTmpl, pretty_print=True),
                   etree.tostring(elemDesc, pretty_print=True)))
                return False

            desc_allowed_range_max = desc_allowed_range[0].xpath('upnp:maximum', ns)
            if len(desc_allowed_range_max) > 0 and tpl_state_var['allowedValueRange'].has_key('maximum') and \
               tpl_state_var['allowedValueRange']['maximum'] != desc_allowed_range_max[0].text.strip():
                louie.send('UPnT.infoMessage', None, "Value for maximum doesn't match! (%s::%s)\n%s\n%s" % \
                  (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion, etree.tostring(elemTmpl, pretty_print=True),
                   etree.tostring(elemDesc, pretty_print=True)))
                return False

            desc_allowed_range_step = desc_allowed_range[0].xpath('upnp:step', ns)
            if len(desc_allowed_range_step) > 0 and tpl_state_var['allowedValueRange'].has_key('step') and \
               tpl_state_var['allowedValueRange']['step'] != desc_allowed_range_step[0].text.strip():
                louie.send('UPnT.infoMessage', None, "Value for step doesn't match! (%s::%s)\n%s\n%s" % \
                  (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion, etree.tostring(elemTmpl, pretty_print=True),
                   etree.tostring(elemDesc, pretty_print=True)))
                return False

        if elemDesc.attrib.has_key('sendEvents') and tpl_state_var['sendEvents'] != elemDesc.attrib['sendEvents']:
            louie.send('UPnT.infoMessage', None, "Values for sendEvents don't match! (%s::%s)\n%s\n%s" % \
              (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion, etree.tostring(elemTmpl, pretty_print=True),
               etree.tostring(elemDesc, pretty_print=True)))
            return False

        return True

    def validateDescriptionContent(self, doc):
        """
        Now that the service description is well-formed, check the contents of
        it.

        @param doc: The description of the service as C{lxml.etree}.
        """

        self.info('validateDescriptionContent')

        template_file = self.templates_dir + 'service/' + self._serviceType + self._serviceVersion + '.xml'
        if not path.isfile(template_file):
            self.debug('no service template available (%s)' % template_file)
            raise MissingTemplateException('no service template available (%s)' % template_file)

        ns = {'upnp': 'urn:schemas-upnp-org:service-1-0'}
        service_template = etree.parse(template_file)

        # get names of actions and if they're required or not
        template_action_list = service_template.xpath('actionList/action')

        description_action_list = doc.xpath('upnp:actionList/upnp:action', ns)

        try:
            self.checkRequiredActions(template_action_list, description_action_list, ns)
        except MissingRequiredActionException, error:
            louie.send('UPnT.infoMessage', None, error)
        else:
            #louie.send('UPnT.infoMessage', None, 'All required actions implemented by %s::%s' % \
            #           (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion))
            pass

        try:
            self.checkActionSyntax(template_action_list, description_action_list, ns)
        except SyntaxError, error:
            louie.send('UPnT.infoMessage', None, error)
        else:
            #louie.send('UPnT.infoMessage', None, 'All implemented actions are syntactically correct (%s::%s)' % \
            #           (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion))
            pass

        # get names of state variables and if they're required or not
        template_state_var_list = service_template.xpath('serviceStateTable/stateVariable')

        description_state_var_list = doc.xpath('upnp:serviceStateTable/upnp:stateVariable', ns)

        try:
            self.checkRequiredStateVars(template_state_var_list, description_state_var_list, ns)
        except MissingRequiredStateVariableException, error:
            louie.send('UPnT.infoMessage', None, error)
        else:
            #louie.send('UPnT.infoMessage', None, 'All required state variables published by %s::%s' % \
            #           (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion))
            pass

        if self.checkStateVariableSyntax(template_state_var_list, description_state_var_list, ns):
            #louie.send('UPnT.infoMessage', None, 'All published state variables are syntacticaly correct (%s::%s)' % \
            #           (self.parentDevice.UUID, self._serviceType + ':' + self._serviceVersion))
            pass

    def validateDescriptionContentFailed(self, failure, url):
        """
        An error occured while checking the content of the service description.

        @param failure: Message about the failure that happened.
        @param url: URL where the description should be located.
        @param d: C{Deferred} that was used to process the description. Used to
        tell the main L{UPnT} instance that the processing chain of this
        C{Deferred} was executed.

        @return: The failure so that the error chain continues and the other
        errbacks are called.
        """

        self.info('validateDescriptionContentFailed')
        self.warning("error requesting ", url)
        self.info(failure)

        return failure

    def startControlTesting(self, result, d):
        """
        Invoking all implemented actions.

        @param result: The result from the previous function in the processing
        chain (not used).
        @param d: C{Deferred} that was used to process the description. Used to
        tell the main L{UPnT} instance that the processing chain of this
        C{Deferred} was executed.
        """

        self.serverControl.startTesting()
        #louie.send('UPnT.ending_deferred', None, d)

    def startControlTestingFailed(self, failure, url, d):
        """
        An error occured while invoking the actions.

        @param failure: Message about the failure that happened.
        @param url: URL where the description should be located.
        @param d: C{Deferred} that was used to process the description. Used to
        tell the main L{UPnT} instance that the processing chain of this
        C{Deferred} was executed.
        """

        self.info('startControlTestingFailed')
        self.info(failure)
        #louie.send('UPnT.ending_deferred', None, d)

    def serviceExpired(self):
        """
        Print a message that the announcement of this service has expired.
        """

        self._expirationNotificationFunc = None
        louie.send('UPnT.infoMessage', None, 'Service announcement of %s:%s has expired!' % (self._serviceType, self._serviceVersion))


class MissingUrlException(StandardError):
    """
    Exception that represents a missing URL in a device or service description.
    """

    def __init__(self, error):
        """
        Initialise the exception and set the error message.

        @param error: Error message about the problem that caused this exception
        to be raised (C{str}).
        """

        self.error = error
        """Holds the error message of this exception."""

    def __str__(self):
        """
        Get a string representation of this exception.

        @return: A string representation of this exception.
        """
        return repr(self.error)


class MissingRequiredActionException(StandardError):
    """
    Exception that represents a missing action in a service description.
    """

    def __init__(self, missingAction, serviceName):
        """
        Initialise the exception and set the name of the missing action and
        the name of the service where the action is missing.

        @param missingAction: Name of the variable that is missing (C{str}).
        @param serviceName: Name of the service where the action is missing
        (C{str}).
        """

        self.missingAction = missingAction
        """Name of the missig action."""
        self.serviceName = serviceName
        """Name of the service where the actiion is missing."""

    def __str__(self):
        """
        Get a string representation of this exception.

        @return: A string representation of this exception.
        """
        return 'Required action %s not implemented by %s' % (self.missingAction, self.deviceName)


class SyntaxError(StandardError):
    """
    Exception that represents wrong syntax in action description. The concrete
    message about why the exception was raised is stored in L{message}.
    """

    def __init__(self, message, actionName, serviceName):
        """
        Initialise the exception.

        @param message: Message about what caused the exception (C{str}).
        @param actionName: Name of the action that is syntactically incorrect
        (C{str}).
        @param serviceName: Name of the service where the action belongs to
        (C{str}).
        """

        self.message = message
        """The message about the actual syntax error."""
        self.actionName = actionName
        """The name of the action."""
        self.serviceName = serviceName
        """The name of the service"""

    def __str__(self):
        """
        Get a string representation of this exception.

        @return: A string representation of this exception.
        """
        return '%s (action %s, device %s)' % (self.message, self.actionName, self.serviceName)


class MissingRequiredStateVariableException(StandardError):
    """
    Exception that represents a missing state variable in a service description.
    """

    def __init__(self, missingVariable, serviceName):
        """
        Initialise the exception and set the name of the missing variable and
        the name of the service where the variable is missing.

        @param missingVariable: Name of the variable that is missing (C{str}).
        @param serviceName: Name of the service where the variable is missing
        (C{str}).
        """

        self.missingVariable = missingVariable
        """The name of the missing variable."""
        self.serviceName = serviceName
        """The name of the service."""

    def __str__(self):
        """
        Get a string representation of this exception.

        @return: A string representation of this exception.
        """
        return 'Required state variable %s not implemented by %s' % (self.missingVariable, self.serviceName)


class MissingTemplateException(StandardError):
    """
    Exception that represents a missing template file.
    """

    def __init__(self, error):
        """
        Initialise the exception and set the error message.

        @param error: Error message about the problem that caused this exception
        to be raised (C{str}).
        """

        self.error = error
        """Holds the error message of this exception."""

    def __str__(self):
        """
        Get a string representation of this exception.

        @return: A string representation of this exception.
        """
        return repr(self.error)

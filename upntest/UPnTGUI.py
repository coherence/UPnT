"""
Base class for the GUI.

@license: Licensed under the MIT license
    http://opensource.org/licenses/mit-license.php
@author: Michael Weinrich <testsuite@michael-weinrich.de>
@copyright: Copyright 2007, Michael Weinrich
"""

import time
import sys
import os

from twisted.internet import reactor, task, defer
from twisted.python.filepath import FilePath

import gtk
import gobject

from coherence import log

from upntest.base import UPnT
import upntest
import louie


class UPnTGUI(log.Loggable):

    logCategory = 'UPnT_GUI'
    """Debug category for the GUI class."""

    UPnT = None
    """Class variable for a single instance of UPnT."""

    def __init__(self, config):
        """
        Create the window and setup the underlying UPnT instance.
        """

        # get all the signals
        louie.connect(self.updateMessageBox, 'UPnT.infoMessage', louie.Any, weak=False)
        louie.connect(self.updateMessageBox, 'UPnT.errorMessag', louie.Any, weak=False)
        louie.connect(self.updateUpnpDevices, 'UPnT.host_discovery_end', louie.Any, weak=False)

        # Try to locate the glade file
        gladefile = "upntest.glade"
        tryfiles = [gladefile,
                    os.path.join(os.path.dirname(sys.argv[0]), gladefile),
                    config.get('glade_dir', '') + gladefile,
                    FilePath(__file__).parent().child('upntest.glade').path]
        for file in tryfiles:
            if os.access(file, os.F_OK):
                gladefile = file
                break
        self.windowname = "windowMain"
        self.wTree = gtk.glade.XML(gladefile, self.windowname)
        self.window = self.wTree.get_widget(self.windowname)

        dic = {
            "onDestroy": self.shutdown,
            "onExecuteTest": self.executeTests,
            "onToolButtonQuit": self.shutdown,
            "onActivateDeviceRow": self.showDetails
        }
        self.wTree.signal_autoconnect(dic)

        # TreView for the devices
        gfx_dir = config.get('gfx_dir', FilePath(__file__).parent().parent().path)
        if not gfx_dir.endswith('/'):
            gfx_dir += '/'
        self.device_icon = gtk.gdk.pixbuf_new_from_file_at_size(gfx_dir + 'gnome-fs-client.svg', 24, 24)
        self.service_icon = gtk.gdk.pixbuf_new_from_file_at_size(gfx_dir + 'icon_webservice.gif', 24, 24)
        self.getWidget("treeviewDeviceTree").set_headers_visible(False)
        imgrenderer = gtk.CellRendererPixbuf()
        textrenderer = gtk.CellRendererText()
        textrenderer.set_property('xalign', 0.0)
        column = gtk.TreeViewColumn()
        column.pack_start(imgrenderer, False)
        column.pack_start(textrenderer, True)
        column.set_attributes(imgrenderer, pixbuf=0)
        column.set_attributes(textrenderer, text=1)
        self.getWidget("treeviewDeviceTree").append_column(column)

        # TreeModel for device tree
        self.deviceTreeModel = gtk.TreeStore(gtk.gdk.Pixbuf,
                                             str,
                                             object)
        self.getWidget("treeviewDeviceTree").set_model(self.deviceTreeModel)

        """Device var that holds the device that is tested when the Execute
        Tests button is hit"""
        self.serviceToTest = None

        # TreeView for information about selected device
        textrenderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn()
        column.pack_start(textrenderer, True)
        column.set_attributes(textrenderer, text=0)
        column.set_title('Name')
        column.set_min_width(150)
        textrenderer = gtk.CellRendererText()
        self.getWidget("treeviewDetails").append_column(column)
        column = gtk.TreeViewColumn()
        column.pack_start(textrenderer, True)
        column.set_attributes(textrenderer, text=1)
        column.set_title('Value')
        self.getWidget("treeviewDetails").append_column(column)

        # ListStore for the device and service details
        model = gtk.ListStore(str, str)
        self.getWidget("treeviewDetails").set_model(model)

        # buffer of the textview at the bottom showing all messages
        self.messageTextBuffer = self.getWidget('textviewMessages').get_buffer()

        # we are in GUI mode
        config['GUI'] = True

        # initialize UPnT
        self.UPnT = UPnT(config)

    def getWidget(self, name):
        return self.wTree.get_widget(name)

    def run(self):
        self.window.show()
        gobject.idle_add(self.refresh)
        gtk.main()

    def shutdown(self, widget=None):
        reactor.stop()
        gtk.main_quit()

    def updateUpnpDevices(self, root_device):
        """
        Update the device tree after inserting a new device or service node.
        """
        self.debug('Updating tree...')

        # no root_device -> don't update
        if not root_device.isRootDevice():
            return

        model = self.getWidget("treeviewDeviceTree").get_model()
        if model is None:
            return
        iter = model.get_iter_first()
        current_parent = None

        while iter is not None:
            cur_dev = model.get_value(iter, 2)
            if cur_dev.ip == root_device.host.ip:
                current_parent = iter
                break
            iter = model.iter_next(iter)

        if current_parent == None:
            current_parent = model.append(None, [self.device_icon, root_device._host.ip, root_device.host])
        else:
            children_iter = model.iter_children(current_parent)
            result = children_iter is not None
            while result:
                result = model.remove(children_iter)

        for uuid, device in root_device.host._serverDevices.items():
            if device.isRootDevice():
                device_iter = model.append(current_parent, [self.device_icon, device.deviceType + ':' + device.deviceVersion, device])
                self.addSubDevicesToTree(model, device_iter, device)
                self.addServicesToTree(model, device_iter, device)

    def addSubDevicesToTree(self, model, device_iter, device):
        """
        Add all subdevices and their services to the tree
        """
        self.debug('addSubdeviceToTree')
        for uuid, subdevice in device.subDevices.items():
            subdevice_iter = model.append(device_iter, [self.device_icon, subdevice.deviceType + ':' + subdevice.deviceVersion, subdevice])
            self.addSubDevicesToTree(model, subdevice_iter, subdevice)
            self.addServicesToTree(model, subdevice_iter, subdevice)

    def addServicesToTree(self, model, device_iter, device):
        """
        Add all services of a device to the tree
        """
        self.debug('addServicesToTree')
        for uuid, service in device._services.items():
            model.append(device_iter, [self.service_icon, service.serviceType + ':' + service.serviceVersion, service])

    def executeTests(self, widget, data=None):
        """
        Run the control test on a device.
        """
        self.debug('executeTests')
        if self.serviceToTest is not None:
            self.debug('startControlTesting')
            self.serviceToTest.startControlTesting(None, defer.succeed(None))

    def updateMessageBox(self, message):
        """
        Add the received message to the text box.
        """
        if self.messageTextBuffer:
            self.messageTextBuffer.insert(self.messageTextBuffer.get_end_iter(), message + '\n')
            self.refreshUI()

    def showDetails(self, treeview):
        """
        Show details of a device or service in the TreeView on the right
        """
        selected_treemodel, selected_iter = treeview.get_selection().get_selected()
        model = self.getWidget("treeviewDetails").get_model()
        model.clear()
        object = selected_treemodel.get_value(selected_iter, 2)
        if isinstance(object, upntest.device.ServerDevice):
            device = object
            model.append(['Device Type', device.deviceType + ':' + device.deviceVersion])
            model.append(['UUID', device.UUID])
            model.append(['Description URL', device.location])
        elif isinstance(object, upntest.service.Service):
            service = object
            self.serviceToTest = service
            model.append(['Service Type', service.serviceType + ':' + service.serviceVersion])
            model.append(['scpdURL', service.scpdUrl])
            model.append(['controlURL', service.controlUrl])
            model.append(['eventSubURL', service.eventSubUrl])

    def refreshUI(self):
        """
        Refresh the UI to show changes of the controls to the user.
        """
        while gtk.events_pending():
            gtk.main_iteration()
        #reactor.callLater(1, self.refreshUI)

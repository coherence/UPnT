"""
Base class of the UPnP Test Tool.

@license: Licensed under the MIT license
    http://opensource.org/licenses/mit-license.php
@author: Michael Weinrich <testsuite@michael-weinrich.de>
@copyright: Copyright 2007, Michael Weinrich
"""

# external includes
import louie
from twisted.internet import reactor, defer
# Coherence includes
from coherence.base import Coherence
from coherence import log
# local includes
from upntest.host import Host


class UPnT(log.Loggable):
    """
    Main class of the test tool. All messages from UPnP devices are send to
    this class which is distibuting the messages to the right object in the
    hierarchy.
    """

    logCategory = 'UPnT'
    """Set a log category different to the Coherence one. All log categories
    of the upntest package will also start with C{UPnT_}."""

    def __init__(self, config={}):
        """
        Initialise the main class.

        During initialisation the main Coherence instance is created and all
        signals are connected to local funtions for processing of received
        packets.

        @param config: A C{dict} that holds all information of the config file
        in the same hierarchical manner that is used in the config file itself.
        """

        self._knownHosts = {}
        """All known hosts that have sent any kind of discovery packet."""

        self._runningDeferreds = []
        """A C{list} that stores all currently running C{Deferred}s."""

        self._hostsToRemove = []
        """A C{list} of hosts that already sent goodbye messages (not used yet).
        """

        self._config = config
        """Stores the given config options to pass it on to other objects who
        need it."""

        self._coherence = Coherence(self._config)
        """The C{Coherence} instance that is created based on the given config
        C{dict}."""

        self.info('connecting signals...')
        louie.connect(self.datagramReceived, 'UPnP.SSDP.datagram_received', louie.Any, weak=False)
        louie.connect(self.datagramReceived, 'UPnP.msearch_datagram_received', louie.Any, weak=False)
        #louie.connect(self.hostRemoved, 'UPnP.host_removed', louie.Any, weak=False)
        if not config.get('GUI', False):
            louie.connect(self.showInfoMessage, 'UPnT.infoMessage', louie.Any, weak=False)
            louie.connect(self.showErrorMessage, 'UPnT.errorMessage', louie.Any, weak=False)
            louie.connect(self.eventMessage, 'UPnT.event.notify_incorrect', louie.Any, weak=False)
            louie.connect(self.eventMessage, 'UPnT.event.subscription_incorrect', louie.Any, weak=False)
        louie.connect(self.addDeferredToList, 'UPnT.running_deferred', louie.Any, weak=False)
        louie.connect(self.removeDeferredFromList, 'UPnT.ending_deferred', louie.Any, weak=False)
        self.info('signals connected, waiting for signals...')
        #reactor.addSystemEventTrigger('before', 'shutdown', self.waitForShutdown)

    def eventMessage(self, message, list):
        self.showErrorMessage(message, ', '.join(list))

    def datagramReceived(self, data, ip, port):
        """
        Process the received SSDP datagram.

        First check if sending host exists in host list. If not create it. Then
        pass on the received packet data to the
        L{dispatchDiscoveryPacketData<Host.dispatchDiscoveryPacketData>} of this
        L{Host} object.

        @param data: A C{str} containing all the packet data.
        @param ip: The IP of the sending host (C{str}).
        @param port: The port to which this packet was sent to. Not used.
        """

        # devices on the local host are ignored
        if ip == self._coherence.hostname:
            return

        #if ip == '192.168.82.1':
        #    return

        if not self._knownHosts.has_key(ip):
            self._knownHosts[ip] = Host(ip, self._config, self._coherence)
            self.info('Host added', ip)
        self.debug('Calling dispatchDiscoveryPacketData')
        #try:
        self._knownHosts[ip].dispatchDiscoveryPacketData(data)
        #except Exception, error:
        #   self.warning(error)

    def hostRemoved(self, ip):
        """
        Remove host from the known hosts list.

        Host with IP sent goodbye messages so it will be removed from the known
        host list.

        @param ip: IP address of the host that is to be removed. (C{str})
        """

        self.info('hostRemoved(%r)' % ip)
        self._hostsToRemove.append(ip)

    def showInfoMessage(self, message):
        """
        Show info messages giving hints on better implementation.

        Messages sent through this channel don't define the tested device as
        invalid.

        @param message: Message that is to be displayed. (C{str})
        """
        print message

    def showErrorMessage(self, type, message):
        """
        Show error messages genereated by test routines.

        Messages sent through ths channel define the tested device as invalid.

        @param type: Type of error that occured. (C{str})
        @param message: Message explaining the error. (C{str})
        """
        print type + ': ' + message

    def waitForShutdown(self):
        """
        Wait for all Deferreds before reactor shutdown.

        Used to wait for all Deferreds to end before the reactor shuts down.

        @return: None if no C{Deferred}s are running or a C{DeferredList} if
        at least one C{Deferred} is still running.
        """
        self.info('Shutting UPnT down...')

        if len(self._runningDeferreds) == 0:
            return None
        else:
            return defer.DeferredList(self._runningDeferreds)

    def addDeferredToList(self, d):
        """
        Add the given Deferred to the list of currently running Deferreds.

        @param d: C{Deferred} that should be added to the list.
        """
        self._runningDeferreds.append(d)

    def removeDeferredFromList(self, d):
        """
        Remove the given Deferred from the list of currently running Deferreds.

        @param d: C{Deferred} that should be removed from the list.
        """
        self._runningDeferreds.remove(d)

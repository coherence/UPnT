"""
Module for utility functions.

@license: Licensed under the MIT license 
    http://opensource.org/licenses/mit-license.php
@author: Michael Weinrich <testsuite@michael-weinrich.de>
@copyright: Copyright 2007, Michael Weinrich
"""

from lxml import etree

from coherence.extern.logger import Logger
log = Logger('UPnT.utils')

from setuptools import setup, find_packages

from upntest import __version__

setup(
    name="UPnT",
    version=__version__,
    description="""UPnT - Test suite for UPnP devices""",
    long_description="""In preparation""",
    author="Michael Weinrich",
    author_email='testsuite@michael-weinrich.de',
    license="MIT",
    packages=find_packages(),
    scripts=['bin/upntest', 'bin/upntest-gui'],
    install_requires=['Coherence', 'lxml', 'Twisted', 'Louie', 'pygtk'],
    url="http://coherence.beebits.net",
    #download_url = 'https://coherence.beebits.net/download/Coherence-%s.tar.gz' % __version__,
    download_url='',
    keywords=['UPnP'],
    classifiers=['Development Status :: 4 - Beta',
                   'Environment :: Console',
                   'Environment :: Web Environment',
                   'License :: OSI Approved :: MIT License',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                ],

    entry_points="""""",

    package_data={
        'coherence': ['xmlfiles/device/*.xml',
                      'xmlfiles/service/*.xml',
                      'schemas/*.xsd']
    }
)

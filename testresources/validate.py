from lxml import etree
import sys

try:
    xmlschema_doc = etree.parse('/home/micxer/development/UPnT/testresources/service.xsd')
except Exception, e:
    print e.error_log

try:
    xmlschema = etree.XMLSchema(xmlschema_doc)
except Exception, e:
    print e.error_log
    sys.exit()

doc = etree.parse('/home/micxer/development/UPnT/testresources/ConnectionManager.xml')
if xmlschema.validate(doc) == 1:
    print 'Valid!'
else:
    print 'Invalid!'
    error = xmlschema.error_log.last_error
    print error
